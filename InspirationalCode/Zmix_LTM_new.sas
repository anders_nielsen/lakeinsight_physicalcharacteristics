/*Analyzing temperature profiles to calculate Zmix, epi, meta and hypolimnion depths and water column stability*/
proc sort data=Clea_exo.LTM_profiles;
   by site date;
run;
/*1- Caculate min and max values for Temp (Te, Th)*/
data profiles;
  set Clea_exo.LTM_profiles;
  if depth<10 then delete;
run;
proc sort data=profiles;
   by site date;
run;
proc means data=profiles noprint;
   var temp_smooth;
   by site date;
   output out=exo01 min=th max=te;
run;
proc sort data=exo01;
   by site date;
run;

/*2.- Merge Th, Te values with other data*/
data exo02;
   merge  profiles exo01;
   by site date ;
   drop _freq_ _type_;
run;

/*3.- Model the TEMP vertical profile by non-linear fitting*/
proc nlin data=exo02 noprint;
   parms a_temp=0.1 n_temp=1;
   bounds 30>a_temp>0.01, 30>n_temp>0.1;
   model temp_smooth=th+(te-th)*((1/(1+((a_temp*depth)**n_temp)))**(1-(1/n_temp)));
   by site date;
   output out=fit_out_temp parms=a_temp n_temp p=pred_temp r=res sse=Rsumsq;
run;
quit;

/*4.- Calculate R2 of the TEMP vertical model */
proc sort data=fit_out_temp;
   by site date ;
run;
data tot1;
   merge exo02 fit_out_temp;
   by site date ;
proc means css noprint data=tot1;
   by site date ;
   var temp_smooth;
   output out=cssdata_temp css=css;
run;
data tot2;
   merge tot1 cssdata_temp;
   by site date ;
   if css ne 0 then r2_temp=(css-Rsumsq)/css;
   drop css rsumsq _type_ _freq_ res;
run;

/*5.- Average values  */
proc sort data=tot2;
   by site date;
run;
proc means noprint data=tot2;
   var pred_temp temp_smooth th te a_temp n_temp r2_temp;
   by site date ;
   output out=bv mean=pred_temp temp_smooth th te a_temp n_temp r2_temp;
run;

/*6.- Generate table for high resolution vertical data (0.1m) */
data bv1;
   set bv;
   do z=0.5 to 110 by 0.1;
   by site date;
   if n_temp=. then delete;
   output;
   end;
run;

/*7.- Generate modelled 0.1m-intervals temp profiles and calculate Zthermocline*/
data bv2;
   set bv1;
   by site date;
   Tz=Th+(Te-Th)*((1/(1+((a_temp*z)**n_temp)))**(1-(1/n_temp)));
   p=(1-6.63*10**(-6)*(tz-4)**2);                       /* density (kg/L) From Temp*/
   m=1-(1/n_temp);

   zthermocline=(m**(m-1))/a_temp;
   if zthermocline>=110 then zthermocline=110;
   if zthermocline=. then zthermocline=110;
run;
proc sort data=bv2;
        by site date z;
run;

/*8.- Calculate density gradient and Zmix */
data zmix;
   set bv2;
   pdiff=dif(p);
   if z<=0.5 then pdiff=0;
   *if pdiff/0.1>=0.00005 then zmix=z;
   if (pdiff/0.1)>=0.005/1000 then zmix=z;  *zmix is the shallowest depth with density fradient >= 0.07 kg m-3 m-1 (the 0.1division is because the pdif is calculated at z increments of 0.1m);

   if zmix=. then zmix=110;
run;

/*9.- Average Zmix and Zthermocline for each day*/
proc means noprint data=zmix ;
      var zmix zthermocline;
      by site date ;
          output out=Zmix1 min=zmix zthermocline;
   run;
proc sort data=bv2;
   by site date ;
run;

/*10.- Merge temp_data with Zmix1_data */
data bv3;
        merge bv2 Zmix1;
        by site date ;
run;

/*11.- Average data for each day  */
proc sort data=bv3;
   by site date ;
run;
proc means noprint data=bv3;
   var pred_temp temp_smooth p zmix zthermocline te th n_temp a_temp r2_temp;
   by site date;
   output out=bv4 mean=pred_temp temp_smooth p0 zmix zthermocline te th n_temp a_temp r2_temp;
run;

/*12.- Calculates Brunt-Vaisala around Zthermocline*/
data bv5;
   set bv4;
   Tz_o=Th+(Te-Th)*((1/(1+((a_temp*zthermocline-0.5)**n_temp)))**(1-(1/n_temp)));       /*estima superior de T a la termoclina */
   Tz_u=Th+(Te-Th)*((1/(1+((a_temp*zthermocline+0.5)**n_temp)))**(1-(1/n_temp)));       /*estima inferior de T a la termoclina */
   pz_o=1-6.63*10**-6*(Tz_o-4)**2;
   pz_u=1-6.63*10**-6*(Tz_u-4)**2;
   p_diff=pz_o-pz_u;
   BV=(-(9.82/p0)*(p_diff/1))**0.5;
   N2=BV**2;
   if te<4 then BV_korr=0;
   if te>=4 and te>=th then BV_korr=BV*1;
   if te>4 and te<th then BV_korr=0;
   if BV_korr<0.05 then ztherm=110; *0.06;
   if BV_korr>=0.05 then ztherm=zthermocline; *0.06;
   T_zmix=Th+(Te-Th)*((1/(1+((a_temp*zmix)**n_temp)))**(1-(1/n_temp)));
run;

/*13.- Correct Zmix and store results in new table*/
data Clea_exo.exo_zmix_profile_ltm;
   set bv5;
   if ztherm=110 then zmix=110;         *changed order so that dzmix accouts for corrected zmix;
   if ztherm<110 then zmix=1*zmix;
   if ztherm<zmix then ztherm=zmix;

   dzmix=zmix-lag(zmix);
   met_width=ztherm-zmix;
   Zlow_met=ztherm+met_width;
   if zlow_met>100 then zlow_met=110;
   week=week(date);
   year=year(date);
   month=month(date);
   day=day(date);
   zmix_neg=-1*zmix;
   ztherm_neg=-1*ztherm;
   Zlow_met_neg=-1*Zlow_met;
   julian1=juldate('01jan15'd);                                                 /*first day of the year of your investigation*/
   julian=juldate(date);
   DOY=julian-julian1+1;                                                                /*day of year*/
   keep site date zmix zthermocline ztherm T_zmix bv N2 dzmix te th
   n_temp a_temp r2_temp met_width Zlow_met week year month day DOY julian1 julian juldate zmix_neg ztherm_neg Zlow_met_neg;
run;

proc gplot data=Clea_exo.exo_zmix_profile_ltm;
   TITLE 'Thermal stratification';
   by site;
   symbol1  value=none interpol=join c=blue ;
   symbol2  value=none interpol=join c=red ;
   symbol3  value=none interpol=join c=green ;
   axis1  label=('time');
   axis2  order=(0 to -110 by 5) label=(angle=90 'Depth(m)');
   plot  zmix_neg*date ztherm_neg*date zlow_met_neg*date
   / legend haxis=axis1 vaxis=axis2 overlay;
run;
quit;
proc gplot data=Clea_exo.exo_zmix_profile_ltm;
   TITLE 'Top and bottom temperature';
   by site;
   symbol1  value=none interpol=join c=blue ;
   symbol2  value=none interpol=join c=red ;
   symbol3  value=none interpol=join c=green ;
   axis1  label=('time');
   axis2  order=(20 to 30 by 1) label=(angle=90 'Temperature (oC)');
   plot  te*date th*date
   / legend haxis=axis1 vaxis=axis2 overlay;
run;
quit;
proc gplot data=Clea_exo.exo_zmix_profile_ltm;
   TITLE 'Water column stability ';
   by site;
   symbol1  value=none interpol=join c=blue ;
   symbol2  value=none interpol=join c=red ;
   symbol3  value=none interpol=join c=green ;
   axis1  label=('time');
   axis2  order=(0 to 0.01 by 0.001) label=(angle=90 'N2');
   plot  N2*date
   / legend haxis=axis1 vaxis=axis2 overlay;
run;
quit;

data zmix2;
   set Clea_exo.exo_zmix_profile_ltm;
   keep site date zmix ztherm zlow_met;
run;

data Clea_exo.exo_zmix_figs_ltm;
  merge profiles zmix2;
  by site date;
run;

proc gplot data=Clea_exo.exo_zmix_figs_ltm;
   TITLE 'Temp profiles with metalimnion';*;
   by site date;
   symbol1  value=star interpol=join c=red;
   symbol2  value=star interpol=join c=blue;
   symbol3  value=triangle interpol=none c=black;
   symbol4  value=dot interpol=none c=green;
   symbol5  value=cross interpol=none c=green ;
   axis1  order=(0 to 110 by 1) label=('depth');
   axis2  order=(23 to 30 by 1) label=(angle=90 'Temp ');
   plot  Temp_smooth*depth Temp_smooth*zmix Temp_smooth*Ztherm Temp_smooth*Zlow_met
   / legend haxis=axis1 vaxis=axis2 overlay vref=0;
run;
quit;

