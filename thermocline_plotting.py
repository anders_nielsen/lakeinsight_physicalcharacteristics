# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 15:27:45 2021

@author: au302246
"""


import plotly.graph_objs as go
import plotly
import numpy
import scipy.interpolate
import os.path
import pandas as pd
import numpy as np
#axes1.set_ylim(min(Rs[z_var])*0.9, max(Rs[z_var])*1.1)
   

def initIndividualPlotting(resultsDict, targetDate, plotsOut):
    
    if targetDate == "all":
        
        for k, results in resultsDict.items():
            individualPlotting(resultsDict, k, plotsOut)
    
    else:
        for k, results in resultsDict.items():
            if results["date"] == targetDate:
                individualPlotting(resultsDict, k, plotsOut)
            else:
                print("The specified date is not present in the dataset. Please check")
    
    return

def individualPlotting(results, j ,plotsOut):
    
    dateStr = results[j]["date"].replace(':', '_').replace(' ', '__')    
    plotsOutfile = os.path.abspath(os.path.join(plotsOut,dateStr+"_id_"+str(j)+".html"))
        
    trace = []    
        
    trace.append(go.Scatter(
            x=results[j]["analysis_THERMOCLINE"]["cleanData"].Temperature,
            #xaxis = 'x2',
            y=results[j]["analysis_THERMOCLINE"]["cleanData"].Depth,
            name = "cleaned data",
            #fill='tonexty',
            mode= 'lines+markers',
            showlegend = True,
            #visible = "legendonly",
            #hoverinfo ="skip",
            line = dict(
                color = ('black'),
                width = 1,)
            ))
    
    trace.append(go.Scatter(
            x=results[j]["analysis_THERMOCLINE"]["downCastRawData"].Temperature,
            #xaxis = 'x2',
            y=results[j]["analysis_THERMOCLINE"]["downCastRawData"].Depth,
            name = "raw",
            #fill='tonexty',
            mode= 'lines',
            showlegend = True,
            #visible = "legendonly",
            #hoverinfo ="skip",
            line = dict(
                color = ('red'),
                width = 1,)
            ))
            
    
    colors = ["blue","green","orange", "purple"]
    Names = ["Depth of Thermocline (TRM)", "Depth of lower Epilimnion (LEP)", "Depth of upper Hypolimnion (UHY)"]
    
    # plot the depth of TRM data
    for i,depth in enumerate([results[j]["analysis_THERMOCLINE"]["TRM_segment"],results[j]["analysis_THERMOCLINE"]["LEP_segment"],results[j]["analysis_THERMOCLINE"]["UHY_segment"]]):
        value = depth if depth is not None else -999 #,color = colors[i]
    
        trace.append(go.Scatter(
                x=results[j]["analysis_THERMOCLINE"]["downCastRawData"].Temperature,
                #xaxis = 'x2',
                y= pd.Series([value] * len(results[j]["analysis_THERMOCLINE"]["downCastRawData"].Temperature)),
                name = Names[i],
                #fill='tonexty',
                mode= 'lines',
                showlegend = True,
                #visible = "legendonly",
                #hoverinfo ="skip",
                line = dict(
                    color = (colors[i]),
                    width = 2,)
                ))
    
    try:
        for i, seg in enumerate(results[j]["analysis_THERMOCLINE"]["segmentation"]):
            trace.append(go.Scatter(
                x=seg[0],
                #xaxis = 'x2',
                y= np.array(results[j]["analysis_THERMOCLINE"]["cleanData"].Depth[seg[1]]),
                name = "segment: %s" %i,
                #fill='tonexty',
                mode= 'lines',
                showlegend = True,
                #visible = "legendonly",
                #hoverinfo ="skip",
                line = dict(
                    #color = (colors[i]),
                    width = 2,)
                ))
    
    except:
        print("no segments")
    
    
            
    layout = dict(
                autosize=True,
                #title= str(obsvar + "("+var_long_name+")"),
                title= str("Lake inSight"),
                
                #annotations = annotations,               
    
                
                font=dict(size=9),
                plot_bgcolor='rgba(255, 255, 255, 1.0)',
                paper_bgcolor='rgba(255, 255, 255, 1.0)',
                            
                xaxis = dict(title='Temperature (C)', #tickformat ="<b>%a</b> \n %d-%b %H:00",
                             #dtick = 86400000.0,
                             #overlaying = 'x', 
                             side = 'top',
                             showgrid=False,
                             #anchor = 'free',
                             #gridcolor = "white",
                             #gridwidth = 4,
                             showline=True,
                             linecolor = "black",
                             ticks='outside',
                             position= 1.0,
                             #range = [numpy.min(zData), numpy.max(zData)]
                             #range = [dt_offset_str,dt_forecast_max]
                             ),
                       
                yaxis = dict(ticks="outside", title="Depth (m)", showline=True, linecolor = "black", hoverformat = '.2f', range = [np.max(results[j]["analysis_THERMOCLINE"]["downCastRawData"].Depth)*1.1, np.min(results[j]["analysis_THERMOCLINE"]["downCastRawData"].Depth)]),
                #yaxis2 = dict(ticks='', title='Depth(m)', hoverformat = '.2f', autorange="reversed",  nticks=36, domain=[0, 0.48]),
                
                #shapes=shapes,            
                
                
                margin = dict(
                            l = 50,
                            r = 50,
                            b = 50,
                            t = 50,
                            pad = 4
                          )
                )
            
        
    fig = go.Figure(data=trace, layout = layout)
    plotly.offline.plot(fig, auto_open = True, filename=plotsOutfile)
    return

   

#results = results_B
def plotly_individual_Old(results, i):

    config = results[i]
        
    if config["variable_to_use"] == "T":           
        var = config["variable_to_use"]+"_org"

    if config["variable_to_use"] == "R":
        var = config["variable_to_use"]+"_org"

    zData = config["z_org"]
    varData = config[var]
    varLabel = config[str(var+"_label")]

    if config["data_for_analysis"] == "profile_fitted":
    
        if config["variable_to_use"] == "T":           
            var2 = config["variable_to_use"]+"_org"

        if config["variable_to_use"] == "R":
            var2 = config["variable_to_use"]+"_org"

        zData2 = config["z_org"]
        varData2 = config[var2]
        varLabel2 = config[str(var2+"_label")]
    
    try:
        CurT = config["data"]["temporary"]["CurT"]
        dTdz = config["data"]["temporary"]["dTdz"]
    except:
        CurT = None
        dTdz = None
    try:
        fig_title = "id: %s date: %s stratification: %s" %(i, config["date"], config["stratification"])
        fig_subtitle = "reason: %s" %(config["note"])
    except:
        fig_title = None
        fig_subtitle = None
 
    
    trace = []
    shapes = None
    annotations = []
    
    annotations.append(dict(xref='paper',
                                yref='paper',
                                x=0.5, y=1.05,
                                showarrow=False,
                                text =fig_title))
                                
    annotations.append(dict(xref='paper',
                                yref='paper',
                                x=0.5, y=0.0,
                                showarrow=False,
                                text =fig_subtitle))     
    
        
    trace.append(go.Scatter(
        x=varData,
        #xaxis = 'x2',
        y=zData,
        name = "profile",
        #fill='tonexty',
        mode= 'lines+markers',
        showlegend = True,
        #visible = "legendonly",
        #hoverinfo ="skip",
        line = dict(
            color = ('black'),
            width = 1,)
        ))
            
    if config["stratification"] == "no":
        print("No thermocline was detected")
    
    else:
        shapes = [
                  dict(
                  type= 'rect',
                  yref= 'y', y0= config["z_epi_bottom"], y1= config["z_hyp_top"],
                  xref= 'paper', x0= 0, x1= 1,
                  fillcolor = "grey", line = dict(color = "blue", width = 0), opacity = 0.3),
                  
                  dict(
                  type= 'line',
                  yref= 'y', y0= config["z_hyp_top"], y1= config["z_hyp_top"],
                  xref= 'paper', x0= 0, x1= 1,
                  line = dict(color = "grey", width = 1), opacity = 1.0),
                  
                  dict(
                  type= 'line',
                  yref= 'y', y0= config["z_epi_bottom"], y1= config["z_epi_bottom"],
                  xref= 'paper', x0= 0, x1= 1,
                  line = dict(color = "grey", width = 1), opacity = 1.0)
                                    
                  ]
        annotations.append(dict(xref='paper',
                      yref='y',
                      x=1.0, y=config["z_epi_bottom"],
                      xanchor = "right",
                      yanchor = "top",
                      showarrow=False,
                      text =str("Thermocline top: %s" %config["z_epi_bottom"])
                      ))
        
        annotations.append(dict(xref='paper',
                      yref='y',
                      x=1.0, y=config["z_hyp_top"],
                      xanchor = "right",
                      yanchor = "bottom",
                      showarrow=False,
                      text =str("Thermocline Bot: %s" %config["z_hyp_top"])
                      
                      ))
    
        if config["i_1"] or config["i_2"] or config["i_3"] == None:
            
            idents = ["i_1", "i_2", "i_3"]
            colors = ["blue", "orange", "green"]
            
            for ident, color in zip(idents, colors):
            
                trace.append(go.Scatter(
                x= numpy.array(varData[config[ident]]),
                #xaxis = 'x2',
                name = ident,
                y= numpy.array(zData[config[ident]]),
                mode= 'markers',
                showlegend = True,
                #visible = "legendonly",
                #hoverinfo ="skip",
                marker = dict(
                              color = (color),
                              size = 6,
                              )
                ))
            #####################################################################
            # plotting thermocline part:
            #####################################################################
            trace.append(go.Scatter(
                x= varData[config["valid_thermocline"]],
                #xaxis = 'x2',
                name = "thermocline points",
                y= zData[config["valid_thermocline"]],
                mode= 'lines',
                showlegend = True,
                visible = "legendonly",
                #hoverinfo ="skip",
                line = dict(
                    color = ('blue'),
                    width = 1,)
                        ))
            
            surf_SLOPE, surf_INTER, thermo_SLOPE, thermo_INTER, bot_SLOPE, bot_INTER = config["coef"]
                        
            abline(surf_SLOPE, surf_INTER, trace, config, varData, zData, name = "surfFit", color ="blue")
            abline(thermo_SLOPE, thermo_INTER, trace, config, varData, zData, name = "thermoFit", color ="orange")
            abline(bot_SLOPE, bot_INTER, trace, config, varData, zData, name = "bottFit", color ="green")
            
            #####################################################################
            # plotting line analysis:
            #####################################################################
            
            trace.append(go.Scatter(
                x=config["curvature"]["dTdz"],
                xaxis = 'x2',
                y=zData,
                name = "dTdz",
                #fill='tonexty',
                mode= 'lines+markers',
                showlegend = True,
                #visible = "legendonly",
                #hoverinfo ="skip",
                line = dict(
                    color = ('orange'),
                    width = 1,)
                ))
                
            trace.append(go.Scatter(
                x=config["curvature"]["curT"],
                xaxis = 'x2',
                y=zData,
                name = "curT",
                #fill='tonexty',
                mode= 'lines+markers',
                showlegend = True,
                #visible = "legendonly",
                #hoverinfo ="skip",
                line = dict(
                    color = ('red'),
                    width = 1,)
                ))    

            
        else:
            print("no thermocline was detected")
            

    layout = dict(
            autosize=True,
            #title= str(obsvar + "("+var_long_name+")"),
            title= str("Lake inSight"),
            
            annotations = annotations,               

            
            font=dict(size=9),
            plot_bgcolor='rgba(255, 255, 255, 1.0)',
            paper_bgcolor='rgba(255, 255, 255, 1.0)',
                        
            xaxis = dict(title=' ', #tickformat ="<b>%a</b> \n %d-%b %H:00",
                         #dtick = 86400000.0,
                         #overlaying = 'x', 
                         side = 'bottom',
                         showgrid=False,
                         #anchor = 'free',
                         #gridcolor = "white",
                         #gridwidth = 4,
                         showline=True,
                         linecolor = "black",
                         ticks='inside',
                         position= 1.0,
                         #range = [numpy.min(zData), numpy.max(zData)]
                         #range = [dt_offset_str,dt_forecast_max]
                         ),
            
            xaxis2 = dict(title=' ',
                           overlaying = 'x', 
                           side = 'bottom',
                           showgrid=False,
                           #anchor = 'free',
                           #gridcolor = "white",
                           #gridwidth = 4,
                           showline=True,
                           linecolor = "black",
                           ticks='inside',
                           #position= 1.0,
                           #range = [numpy.min(zData), numpy.max(zData)]
                           #range = [dt_offset_str,dt_forecast_max]
                           ),
            
                   
            yaxis = dict(ticks="outside", title="yaxis_title", showline=True, linecolor = "black", hoverformat = '.2f', range = [numpy.max(zData)*1.1, numpy.min(zData)*1.1]),
            #yaxis2 = dict(ticks='', title='Depth(m)', hoverformat = '.2f', autorange="reversed",  nticks=36, domain=[0, 0.48]),
            
            shapes=shapes,            
            
            
            margin = dict(
                        l = 50,
                        r = 50,
                        b = 50,
                        t = 50,
                        pad = 4
                      )
            )

        
    fig = go.Figure(data=trace, layout = layout)
    plotly.offline.plot(fig, filename="test.html")
    
    return


def abline(slope, intercept, trace, config, varData, zData, name, color ):
    """Plot a line from slope and intercept or vertical lines"""
        
    if slope == "vertical line":
        y_vals = numpy.array([numpy.min(zData), numpy.max(zData)])
        
        x_vals = numpy.empty(len(y_vals))
        x_vals.fill(intercept)

    else:
        x_vals = numpy.array([numpy.min(varData), numpy.max(varData)])
        y_vals = intercept + slope * x_vals
        
    print(name)        
    print(x_vals)
    print(y_vals)
    print("****************")
    trace.append(go.Scatter(
                x= x_vals,
                #xaxis = 'x2',
                name = name,
                y= y_vals,
                mode= 'lines',
                showlegend = True,
                #visible = "legendonly",
                #hoverinfo ="skip",
                line = dict(
                    color = (color),
                    width = 1,
                    dash='dash')
                        ))
       
    return



def plotly_temporal(PlotPath,results, var = "mix"):    
 

    date = []
    z_epi_bottom =[]
    z_hyp_top = []
    z_mix = []
    st = []
    T_org = []
    z_all = []
    # getting depths to get the max and min depth
    for key, Rs in results.items():
        z_all.append(Rs["z_raw"])
    
    # use for observations:
    z_all = numpy.array(z_all)
    z_min, z_max = numpy.nanmin(z_all), numpy.nanmax(z_all)
    dz = 0.1
    
    firstKey = list(results.keys())[0]
    
    if results[firstKey]["datasetType"] == "obs":
        intpolDepth = numpy.arange(z_min, z_max+dz, dz)
        z_org = intpolDepth
 
    elif results[firstKey]["datasetType"] == "model":
        z_org = results[firstKey]["z_raw"]
        
    for key, Rs in results.items():
        date.append(Rs["date"])
        z_epi_bottom.append(Rs["analysis_THERMOCLINE"]["LEP_segment"])
        z_hyp_top.append(Rs["analysis_THERMOCLINE"]["UHY_segment"])
        if var == "stability":
            st.append(Rs["analysis_SCHMIDTS"])
        else:
            z_mix.append(Rs["analysis_THERMOCLINE"]["LEP_segment"])
        
        #--------------------obs-----------------------------------#
        if Rs["datasetType"] == "obs":
                           
            T_out = Rs["T_raw"] 
            T_cal = T_out[~numpy.isnan(T_out)]
            z_out = Rs["z_raw"]
            z_cal = z_out[~numpy.isnan(z_out)]

            T_interp_func = scipy.interpolate.interp1d(z_out, T_out, fill_value="extrapolate")
            T_interp = T_interp_func(intpolDepth)
            T_org.append(T_interp)

        #--------------------model-----------------------------------#
        else:    
            T_org.append(Rs["T_raw"])
    
    T_org = numpy.array(T_org)
    
    
    trace = []
    
    if var == "stability":
        y_plot = st
        yaxis_title = "Schmidt Stability (J/m2)"
    else:
        y_plot = z_mix
        yaxis_title = "Mixed layer depth (m)"
    
    trace.append(go.Bar(
        x=date,
        #xaxis = 'x2',
        y=y_plot,
        #fill='tonexty',
        #mode= 'line',
        showlegend = False,
        #visible = "legendonly",
        #hoverinfo ="skip",
       # line = dict(
       #     color = ('rgb(47,79,79)'),
       #     width = 1,)
        ))
  


    cbarlocs = [.75, .25]
    trace.append( go.Heatmap(
            name = "Temperature", #obsvar,
            z= numpy.transpose(T_org),
            x = date,
            xaxis = 'x',
            y = z_org,
            yaxis="y2",
            colorscale='Viridis', #'Portland', #'Viridis',
            zsmooth = 'best',
            colorbar = dict(title= "<sup>o</sup>C" ,
                            titleside ='top',
                            dtick = 2,
                            y=cbarlocs[1],
                            len=0.5,
                            #tickvals =[0.1],
                            tickfont = dict(color='black')                
                            )
            )#, row=1, col=1
            )
    """
    # activate an overlay of the mixing depth on top of the contour-plot
    trace.append(go.Scatter(
        x=date,
        xaxis = 'x1',
        yaxis="y2",
        y=z_mix,
        #fill='tonexty',
        #mode= 'line',
        showlegend = True,
        #visible = "legendonly",
        #hoverinfo ="skip",
        line = dict(
            color = ('rgb(255,255,255)'),
            width = 1)
        ))
    """
    layout = dict(
            autosize=True,
            #title= str(obsvar + "("+var_long_name+")"),
            title= str("Lake InSight"),
            
            font=dict(size=9),
            plot_bgcolor='rgba(255, 255, 255, 1.0)',
            paper_bgcolor='rgba(255, 255, 255, 1.0)',
                        
            xaxis = dict(title=' ', #tickformat ="<b>%a</b> \n %d-%b %H:00",
                             #dtick = 86400000.0,
                             #overlaying = 'x', 
                             side = 'bottom',
                             showgrid=True,
                             #anchor = 'free',
                             gridcolor = "white",
                             gridwidth = 4,
                             #ticks='outside',
                             #position= 0.99,
                             #range = [data_view, data_extract_stop]
                             #range = [dt_offset_str,dt_forecast_max]
                             ),
            
                   
            yaxis = dict(ticks='', title=yaxis_title, hoverformat = '.2f', nticks=36, domain=[0.52, 1]),
            yaxis2 = dict(ticks='', title='Depth(m)', hoverformat = '.2f', autorange="reversed",  nticks=36, domain=[0, 0.48]),
               
            
            
            margin = dict(
                        l = 50,
                        r = 50,
                        b = 50,
                        t = 50,
                        pad = 4
                      )
            )

        
    fig = go.Figure(data=trace, layout = layout)
    #plotly.offline.plot(fig, filename="test.html")
    plotly.offline.plot(fig, filename=os.path.join(PlotPath, str("temporal_"+var+'.html')))
    
            
            
            