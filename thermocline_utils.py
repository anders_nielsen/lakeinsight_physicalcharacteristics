# -*- coding: utf-8 -*-
"""
Created on Tue May 28 09:53:41 2019

@author: au302246
"""
#packages to import:
import os.path
import os
import numpy as np
from collections import defaultdict
from datetime import datetime
import pandas as pd

###############################################################################
# Importing the observed profiles 
###############################################################################

#obsDataset = "O:/Tech_DYAMO-DK/07_CODE/Thermocline/DATA/obs/TEMP.obs"

def read_obs_format(obsDataset):

    dic = defaultdict(lambda: defaultdict(list))
    obsTime = []
    obsDepth = []
    obsValue = []     
    
    input_file = obsDataset #os.path.join(inputfolder, file)    
    
    with open(input_file,'r') as f:
        lines = f.readlines()
        #dic = {}
        for line in lines:
            if line.strip()=='': break
            
            time = line[:19]
            z,val = line[19:].strip().split()
                       
            dic[time]["depth"].append(float(z))
            dic[time]["val"].append(float(val))
    
    for key, val in dic.items():
        
        # handling datetime fmt:
        key_dt = datetime.strptime(key, '%Y-%m-%d %H:%M:%S')
        obsTime.append(key_dt)
        
        d = val["depth"]
        v = val["val"]
        # the values are sorted based on the depth
        v_sorted = [x for _,x in sorted(zip(d,v))]
        d_sorted = sorted(d) 
        
        obsDepth.append(d_sorted)
        obsValue.append(v_sorted)
    
    obsDepthBal = balancedArray(obsDepth)
    obsValueBal = balancedArray(obsValue)
            
    obsData = {"obsTime"  : obsTime,
               "obsDepth" : obsDepthBal,
               "obsValue" : obsValueBal
               }
                             

    return obsData


def ReadHypsograph(hypsDataset):
    
    depthData = []
    areaData = []
    input_file = hypsDataset
    with open(input_file,'r') as f:
        next(f) # skip a line
        lines = f.readlines()
        for line in lines:
            if line.strip()=='': break
            
            depth,area = line.strip().split()
            depthData.append(float(depth))
            areaData.append(float(area))
    
    depthData  = np.array(depthData)
    areaData  = np.array(areaData)
    
    return depthData, areaData


def balancedArray(dataIn):
    row_lengths = []
    for row in dataIn:
        row_lengths.append(len(row))
        max_length = max(row_lengths)
    
    for row in dataIn:
        while len(row) < max_length:
            row.append(np.nan)

    balanced_array = np.array(dataIn)

    return balanced_array


def exportResults(resultDict,plotsOut):

    ids         = [] 
    date        = []
    TRM         = []
    TRMgradient = []
    TRMnote     = []
    LEP         = []
    LEPisStable = []
    LEPgradient = []  
    UHY         = []
    Schmidts   = [] 
    print("Exporting results")    
    for k,v in resultDict.items():
        
        ids.append(k)
        date.append(v["date"])
        TRM.append(v["analysis_THERMOCLINE"]["TRM_segment"])
        TRMnote.append(v["analysis_THERMOCLINE"]["TRM_detection_Note"])
        TRMgradient.append(v["analysis_THERMOCLINE"]["TRM_gradient_segment"])
        LEP.append(v["analysis_THERMOCLINE"]["LEP_segment"])
        try:
            LEPisStable.append(v["analysis_THERMOCLINE"]["LEP_features"]["gradientIsStable_0"])
            LEPgradient.append(v["analysis_THERMOCLINE"]["LEP_features"]["actualGradient_0"])
        except TypeError:
            LEPisStable.append("None")
            LEPgradient.append("None")
        UHY.append(v["analysis_THERMOCLINE"]["UHY_segment"])
        
        try:
            Schmidts.append(v["analysis_SCHMIDTS"])
        except TypeError:
            Schmidts.append("None")
        
        
    
    df = pd.DataFrame(list(zip(ids, date,TRM,TRMnote, TRMgradient,LEP, LEPisStable, LEPgradient, UHY, Schmidts)), columns = ["ids", "date","TRM","TRMnote","TRMgradient","LEP", "LEPisStable", "LEPgradient","UHY", "Schmidts"])
    
    df.to_csv(os.path.join(plotsOut,"data.csv"), index = False)
    
    return


def water_density(temp,sal=0.15):
    """ calculate water density at sea level after Chen & Millero 1986 (Eq 1) L&O 31: 657-662
    """
    units = {"density"  : "kg m^-3", 
             "temp"     : "degree C", 
             "salinity" : "g kg^-1"
             }

    method = 1
    
    if method == 1:
        rho_water = (1000*(1-(temp+288.9414)*(temp-3.9863)**2/(508929.2*(temp+68.12963))))
    
    elif method ==2:
        #Martin and McCutcheon (1999)
        # -- equation 1:
        rho_0 = 999.842594 + 6.793952*10**(-2)*temp - 9.095290*10**(-3)*temp**2 + 1.001685*10**(-4)*temp**3 - 1.120083*10**(-6)*temp**4 + 6.536335e-9*temp**5
         
        # -- equation 2:
        A = 8.24493*10**(-1) - 4.0899e-3*temp + 7.6438*10**(-5)*temp**2 - 8.2467*10**(-7)*temp**3 + 5.3875*10**(-9)*temp**4
          
        # -- equation 3:
        B = -5.72466*10**(-3) + 1.0227*10**(-4)*temp - 1.6546*10**(-6)*temp**2
          
        # -- equation 4:
        C = 4.8314*10**(-4)
           
        # -- equation 5:
        rho_water = rho_0 + A*sal + B*sal**(3/2) + C*sal**2

    elif method ==3:
        rho_water = (0.9998395 + 6.7914e-5*temp - 9.0894e-6*temp**2 + 1.0171e-7*temp**3 - 
                 1.2846e-9*temp**4 + 1.1592e-11*temp**5 - 5.0125e-14*temp**6 + 
                 (8.181e-4 - 3.85e-6*temp + 4.96e-8*temp**2)*sal)*1000 

    return rho_water

def checkingDatetime(config):
    if config["Configuration"]["period"]["request"] == "all": 
        
        dtDict = {"FlagPeriod" :"all"}
        
        print("The entire dataset will be analyzed")
        
        return dtDict
    
    elif config["Configuration"]["period"]["request"] == "period":
                
        try:
            dateExtractStart = datetime.strptime(config["Configuration"]["period"]["start"], '%Y-%m-%d %H:%M:%S')
        except:
            print("wrong format of datetime in configuration start")
            return None
        
        try:
            dateExtractStop = datetime.strptime(config["Configuration"]["period"]["stop"], '%Y-%m-%d %H:%M:%S')
        except:
            print("wrong format of datetime in configuration stop")
            return None        
        
        if dateExtractStart > dateExtractStop:
            print("Please verify that extraction date 'stop' is after 'start'")
            return None
        
        else:
            # continue
            print("Configrations are set to extract data from: %s to %s" %(dateExtractStart,dateExtractStop))
            
            dtDict = {"FlagPeriod"        : "period",
                      "dateExtractStart"  : dateExtractStart,
                      "dateExtractStop"   : dateExtractStop
                     }
     
            return dtDict           