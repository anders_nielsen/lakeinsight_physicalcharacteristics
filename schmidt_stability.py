# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 15:37:34 2021

@author: Tobias K Andersen

Calculate Schmidt stability (translated from the rLakeAnalyzer package)

"""

import numpy as np
import scipy.interpolate


"""
# Here is just some madeup data. This should 
# seem valid to the Schmidt Stability algorithm. Valid enough at least
wtr = np.array([24,24,24,20,17,12,11,10,10])
depths = np.array([0,1,2,3,4,5,6,7,8])
sal = wtr*0
depths = np.array([1,2,3,4,5,6,7,8,9]) 
bthA = np.array([8,7,6,5,4,3,2,1,0])
bthD = np.array([1,2,3,4,5,6,7,8,9.0])
rhoL = np.array([1,1,1,1,2,3,3,3,3])
should result in a St = 51.9
"""

"""
Another example based on Lake bosumtwi, Ghana:
bthA	=	np.array([48799505,37529170,23934141,16818165,61649])
bthD	=	np.array([0,20,40,50,70])
	
wtr	    =   np.array([30.26,29.96,27.86,27.29,27.07,26.97,26.96,26.96])
depths	=	np.array([0.3,0.5,5,10,20,30,40,50])
sal = wtr*0
"""

# rhoL = None
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

def schmidt_stability(config, datasetType, wtr, depths, bthA, bthD, sal = 0.0, rhoL_in = None):
    """Calculates Schmidts stability (in J/m2)
    
    Parameters
    ----------
    wtr : array
        array of water temperatures (in Celsius)
    depths : array
        array of depths corresponding to the depths (in m) of the wtr
    bthA : array
        array of cross sectional areas (in m2) corresponding to bthD depths
    bthD : array 
        array of of depths (in m) which correspond to areal measures in bthA
    sal : array, optional
        array of salinity (in Practical Salinity Scale units)
    rhoL_in : array, optional
        array of water density (in kg/m3)
    
    Returns
    -------
    St : array
        array of Schmidt stability (in J/m2)
    """
       
    # check same length of wtr and depth
    if len(wtr) != len(depths):
        print("wtr array and depth array are not of the same length!")

    # In case the salinity is specified as an integer (likely not specified) 
    # then we need to create an array of salinities    
    if isinstance(sal,int) or isinstance(sal,float):
        sal = sal*depths
    
    #Calculate water density if not given
    
    NoneType = type(None)
    if type(rhoL_in) != NoneType:
        rhoL = rhoL_in
    #else:
    #    if rhoL_in.any() != None:
    #        rhoL = rhoL_in
    else:
        print("Water density not given, therefore calculated.")
        rhoL = water_density(wtr, sal)
    
    # If data comes from OBS files, we need to turn data around
    # to form the correct format for the calculations
    # we check that by looking at the hypsograph and depths
    if bthA[0] < bthA[-1] and depths[0] > depths[-1] :
        wtr = np.flip(wtr)
        depths = np.flip(depths)
        bthA = np.flip(bthA)
        bthD = np.flip(bthD)
        sal = np.flip(sal)
        rhoL = np.flip(rhoL)    
    
    # we need to filter out potential surface values to avoid near surface
    # noise.
    threshDepth=config["Preprocessing"]["badDepthThreshold"]
    valid = np.array([t >= threshDepth for t in depths], dtype=bool)
                
    depths = depths[valid]
    wtr = wtr[valid]
    sal = sal[valid]
    rhoL = rhoL[valid]
    
    
    #Constants
    g = 9.81
    dz = 0.1
    
    if datasetType == "obs":
        numD = len(wtr)-2
        if max(bthD) > depths[numD]:
            wtr = np.append(wtr, wtr[numD])
            sal = np.append(sal, sal[numD])
            rhoL = np.append(rhoL, rhoL[numD])
            depths = np.append(depths, max(bthD))
        elif max(bthD) < depths[numD]:
            bthD = np.append(bthD, depths[numD])
            bthA = np.append(bthA, 0)        
    
        if min(bthD) < depths[0]:
            wtr = np.insert(wtr, 0, wtr[0])
            sal = np.insert(sal, 0, sal[0])
            rhoL = np.insert(rhoL, 0, rhoL[0])
            depths = np.insert(depths, 0, min(bthD))
    
    if datasetType == "model":
        numD = len(wtr)-2
        if min(bthD) < depths[0]:
            wtr = np.insert(wtr, 0, wtr[0])
            sal = np.insert(sal, 0, sal[0])
            rhoL = np.insert(rhoL, 0, rhoL[0])
            depths = np.insert(depths, 0, min(bthD))
        
    
    Zo = min(depths)
    Io = np.where(depths == Zo)
    Ao = bthA[Io]
    
    #print(wtr)
    #print(depths)
    #print(bthA)
    #print(bthD)
    #print(sal)
    #print(rhoL)
    
    # handle if: (Ao == 0)
            
    #
    layerD = np.arange(min(depths), max(depths)+dz, dz)
    # R script contain interpolation 
    # linearly interporlate : approx(depths, rhoL, layerD)$y
    
    layerP_f = scipy.interpolate.interp1d(depths, rhoL, fill_value="extrapolate") 
    layerP = layerP_f(layerD)
    # linearly interporlate : approx(bthD, bthA, layerD)$y
    layerA_f = scipy.interpolate.interp1d(bthD, bthA, fill_value="extrapolate") 
    layerA = layerA_f(layerD)

    
    #Zcv = layerD %*% layerA / sum(layerA)
    Zcv = sum(layerD*layerA) / sum(layerA)
    #print(str(Zcv))
    
    #St = layerP %*% ((layerD - as.vector(Zcv)) * layerA) * dz * g / Ao
    St = sum(layerP * ((layerD - Zcv) * layerA)) * dz * g / Ao 
    #print("SS")
    #print(str(St))
    
    return St 

def water_density(wtr, sal):
    """Calculates water density based on Martin & McCutcheon and UNESCO
    
    Parameters
    ----------
    wtr : array
        Array of water temperatures (in Celsius)
    sal : array
        Array of salinity (in Practical Salinity Scale units)
    
    Returns
    -------
    rho : array
        Array of water density (in kg/m3)
    """

    # check if wtr and sal is same length?
    #if len(wtr) != len(sal):
    #    print("wtr array and sal array are not of the same length!")
        
    # Determine which method we want to use, initially set both methods to false
    MM = False # Martin & McCutcheon
    UN = False # UNESCO
        
    # specify temperature and salinity range for the calculations
    Trng = np.arange(0,41) # temperature range
    Srng = np.arange(0.5,43.5) # salinity range
    
    # check to see if all values lie within the ranges specified
    if sal.all() < Srng[0]:
        MM = True
    # is this the criteria in 
    elif np.logical_and(wtr > Trng[1], wtr < Trng[-1]):
        if np.logical_and(sal > Srng[1], sal < Srng[-1]):
            UN = True
        else:
            print("Water temp. within range, but salinity out of range.")
            
    # if MM is true we use method of UNESCO (1981)
    if MM:
        rho = (1000*(1-(wtr+288.9414)*(wtr-3.9863)**2/(508929.2*(wtr+68.12963))))
    
    # if UN is true we use method of Martin and McCutcheon (1999)
    elif UN:
        # -- equation 1:
        rho_0 = 999.842594 + 6.793952*10**(-2)*wtr - 9.095290*10**(-3)*wtr**2 + 1.001685*10**(-4)*wtr**3 - 1.120083*10**(-6)*wtr**4 + 6.536335e-9*wtr**5
         
        # -- equation 2:
        A = 8.24493*10**(-1) - 4.0899e-3*wtr + 7.6438*10**(-5)*wtr**2 - 8.2467*10**(-7)*wtr**3 + 5.3875*10**(-9)*wtr**4
          
        # -- equation 3:
        B = -5.72466*10**(-3) + 1.0227*10**(-4)*wtr - 1.6546*10**(-6)*wtr**2
          
        # -- equation 4:
        C = 4.8314*10**(-4)
           
        # -- equation 5:
        rho = rho_0 + A*sal + B*sal**(3/2) + C*sal**2
        
    # if there is a combination of fresh and saline water we need to use a combination of MM and UN
    else:
        # maybe set rho to Nan instead of 0?
        rho = wtr*0
        for idx in np.arange(0,len(rho)):
            rho[idx] = water_density(wtr[idx], sal[idx])
        
        # ensure same dimension as input array ?
    
    return rho

# -- References
# ?? Martin, J.L., McCutcheon, S.C., 1999. Hydrodynamics and Transport ??
# ?? for Water Quality Modeling. Lewis Publications, Boca              ??
# ?? Raton, FL, 794pp. >>
#
#?? Millero, F.J., Poisson, A., 1981. International one-atmosphere    ??
#?? equation of state of seawater. UNESCO Technical Papers in Marine  ??
#?? Science. No. 36. >>
    
    
    
    
