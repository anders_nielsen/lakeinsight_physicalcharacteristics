# LakeInsight_physicalCharacteristics



## About
This program is intended to empower users to easily post process profile data from either observations or model output and provide details on different physical aspects of lakes.
It builds on work from Tom Shatwell as well as the following projects:

https://rdrr.io/cran/rLakeAnalyzer/

http://stormxuwz.github.io/SeabirdCode/

the work is co-developed with Dennis Trolle and Tobias K. Andersen

# Approach to detect thermocline features:

(Description from stormxuwz)
Thermocline:
•	Approximate vertical temperature profiles with linear segments using a piecewise linear representation algorithm, from which stratification patterns can be extracted.
•	The transition zone between the epilimnion and hypolimnion is the metalim-nion, and the horizontal plane within the metalimnion that has the sharpest temperature gradient is calledthe thermocline (TRM)

Approach:
•	The algorithm workflow is:
o	Data preprocessing - aggregate data (i.e., standardize sampling depth intervals), and smooth fluctuations by computing moving averages. 
Step 1: Remove the lake surface data collected at depths less than 3 m. These surface data usually have anomalous spikes.
Step 2: Bin data with a 0.25‐m interval (half of current USEPA‐GLNPO protocols) and average the data in each bin.
Step 3: Smooth the data using a moving average filter with Hann window (Oppenheim et al., 1999), and window size of 2 m. The size of this smoothing window is chosen by trial and error to avoid excessive distortion in the profile data

o	Then the algorithms detect lake stratification features using piecewise linear segmentation algorithms.
TRM, LEP, and UHY are used to represent the thermocline, lower epilimnion, and upper hypolim-nion, respectively

