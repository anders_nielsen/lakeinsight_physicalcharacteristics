# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 09:48:19 2021

@author: au595054
"""

import netCDF4
import numpy as np
import datetime
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy, scipy, matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.optimize import differential_evolution
import warnings
import copy
from collections import defaultdict

# script specific functions
import fitting_profile as cf
import thermocline_utils as t_utils

# schmidt stability
import schmidt_stability as st

# specifying dataset
datasetNC = "O:/Tech_DYAMO-DK/07_CODE/Thermocline/DATA/Bryrup/output.nc"
datasetOBS = "O:/Tech_DYAMO-DK/07_CODE/Thermocline/DATA/obs/TEMP.obs"

datasetType = "model"

dataset = datasetOBS
dataset = datasetNC


if datasetType == "model":
    # import from GOTM model
    with netCDF4.Dataset(dataset) as nc:
        
        z = nc.variables["z"][..., 0, 0]
        h = nc.variables["h"][..., 0, 0]
        temp = nc.variables["temp"][..., 0, 0] # Celsius
        density = nc.variables["rho"][..., 0, 0] # kg/m3
        sal = nc.variables["salt"][..., 0, 0] # g/kg
        bthA = nc.variables["Af"][..., 0, 0] # m2, hypsograph at grid interface
        depths = z * -1
        bthD = depths # m, bathymetry depths is identical to z in model
        #mixed_bot = nc.variables["mld_bott"][..., 0, 0] # bottom mixed layer depth (m)
        #surf_fric = nc.variables["u_taus"][..., 0, 0] # surface water friction (due to wind?) (m/s)
        
        nctime = nc.variables['time']
        time_vals = nctime[:]
        time_unit = nctime.units
        datetime_fmt = (netCDF4.num2date(time_vals, time_unit))         

elif datasetType == "obs":
    # import OBS profiles
    try:
        obsData = t_utils.read_obs_format(dataset) #HERTIL!!!!!!
    
    except Exception as e:
        print("The format of the .obs file is not correct")
        print(e)
        
    datetime_fmt    = obsData["obsTime"]
    z               = obsData["obsDepth"]
    temp            = obsData["obsValue"]
    density         = t_utils.water_density(temp)

# creating dict to hold the results produced
results = {}
for i, d in enumerate(datetime_fmt): # datetime_fmt[date_mask]
    results[i] =    {    
                    "date": d, #d.strftime('%Y-%m-%d'),
                    "note": None,
                    "stratification" : None,    # "yes" / "no"
                    "St": None,
                    "mixed_layer_depth": None
                    }

for i, (T_raw, R_raw, z_raw, depths_raw, sal_raw, bthA_raw) in enumerate(zip(temp, density, z, depths, sal, bthA)):
    
    print("==============================================")
    print("ID: %s" %i)
    print("==============================================")
    
    # deleting possible nan values.
    #T_raw = T_raw[~np.isnan(T_raw)]
    #z_raw = z_raw[~np.isnan(z_raw)]
    #R_raw = R_raw[~np.isnan(R_raw)]
    results[i]["St"] = st.schmidt_stability(T_raw, depths_raw, bthA_raw, depths_raw, sal = sal_raw, rhoL_in = R_raw)
    print("Schmidt stability calculated.")
    results[i]["mixed_layer_depth"] = mixed_bot[i]




