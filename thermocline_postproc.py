# -*- coding: utf-8 -*-
"""
Created on Wed Jun 12 15:18:40 2019

@author: au302246
"""

import datetime
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


date = []
j = []
scenario = []
z_epi_bottom =[]
z_hyp_top = []
category = []
        
analysis_lst = [results_B, results_N, results_M ,results_E]
analysis_lst_nm = ["Baseline", "Near_future", "Medium", "Extreme"]

for dic, nme in zip(analysis_lst, analysis_lst_nm):

    for key, Rs in dic.items():
        j_i = datetime.datetime.strptime(Rs["date"],"%Y-%m-%d").strftime('%j')
        
        mon = datetime.datetime.strptime(Rs["date"],"%Y-%m-%d").month
        if mon > 4 and mon < 10: 
            category.append("DRY") # May to Sep 
        else:        
            category.append("WET") # Oct to Apr
        
        scenario.append(nme)
        z_epi_bottom.append(Rs["z_epi_bottom"])
        z_hyp_top.append(Rs["z_hyp_top"])
        date.append(Rs["date"])
        j.append(j_i)


df = pd.DataFrame(
    {"date":date,
     "j": j,
     'scenario': scenario,
     'category': category,
     'epi_bottom': z_epi_bottom,
     'hyp_top': z_hyp_top
    })



    
plt.bar(df["date"],df["epi_bottom"])    
    
fig, ax = plt.subplots(figsize=(8,6))
for label, df_i in df.groupby('scenario'):
    ax.plot(df_i["date"], df_i["epi_bottom"], label=label)
plt.legend()


k = df[(df.scenario == "Baseline")]
#k = df[(df.scenario == "") & (df.D == 6)]


#plt.plot(k["j"],k["epi_bottom"])
sns.boxplot(x=k.index, y="epi_bottom", data=k)

sns.barplot(x="scenario", hue="category", y="epi_bottom", data=df)





















    