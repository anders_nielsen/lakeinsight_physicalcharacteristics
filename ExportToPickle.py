# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 17:13:36 2022

@author: au302246
"""

import pickle

a = {'hello': 'world'}

with open('filename.pickle', 'wb') as handle:
    pickle.dump(a, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('filename.pickle', 'rb') as handle:
    b = pickle.load(handle)

print(a == b)