# -*- coding: utf-8 -*-
"""
Created on Fri May 24 12:40:06 2019

@author: au302246
"""

import numpy
import datetime
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy, scipy, matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.optimize import differential_evolution
import warnings


# script specific functions
import thermocline_utils as t_utils

#=============================fitting curve to profile

class curve_fitting(object):

    def __init__(self):
    
        return
    
    def fitting_curve_to_profile(self,variable,depth):
    
        self.yData = variable # e.g. temperature or density
        self.xData = depth # depth as positive 
        
        # /*1- Caculate min and max values for Temp (Te, Th)*/
        self.te = numpy.max(self.yData)
        self.th = numpy.min(self.yData)
            
        # generate initial parameter values
        geneticParameters = self.generate_Initial_Parameters()
        
        # curve fit the test data
        fittedParameters, pcov = curve_fit(self.func1, self.xData, self.yData)#, geneticParameters)
        
        print('Parameters', fittedParameters)
        
        modelPredictions = self.func1(self.xData, *fittedParameters) 
        
        absError = modelPredictions - self.yData
        
        SE = numpy.square(absError) # squared errors
        MSE = numpy.mean(SE) # mean squared errors
        RMSE = numpy.sqrt(MSE) # Root Mean Squared Error, RMSE
        Rsquared = 1.0 - (numpy.var(absError) / numpy.var(self.yData))
        print('RMSE:', RMSE)
        print('R-squared:', Rsquared)
    
    
        #/*7.- Generate modelled 0.1m-intervals temp profiles and calculate Zthermocline*/
    
        T_fitted_profile = self.func1(self.xData, *fittedParameters)
        #density (kg/L) From Temp
        R_calculated = t_utils.water_density(T_fitted_profile)
        #print(R_calculated)
                     
        m = 1-(1/fittedParameters[1])
        
        zthermocline= (m**(m-1))/fittedParameters[0]
            
        if zthermocline >= numpy.max(self.xData):
           zthermocline=numpy.max(self.xData)
    
        #/*8.- Calculate density gradient and Zmix */

        R_gradient = numpy.gradient(R_calculated) /  numpy.gradient(self.xData) #* -1.0
        
        #print(R_gradient)
        
        #valid = numpy.array([zi >= 0.5 for zi in self.xData], dtype=int)        
                       
        R_gradient_cor = R_gradient#*valid
        
        #print(R_gradient_cor)
        
        #plt.clf()
        #plt.plot(R_gradient_cor,self.xData)
        #plt.gca().invert_yaxis()
    
        density_fradient = 0.005/1000 #kg m-3 m-1 ; zmix is the shallowest depth with density fradient 
        
        valid = numpy.array([Ri >= density_fradient for Ri in R_gradient_cor], dtype=bool)
        
        zmix = min(self.xData[valid])
               
        #print(R_gradient_cor)
        #print(zmix)
        
        Tz_o = self.func1(zthermocline-0.5, *fittedParameters)
        Tz_u = self.func1(zthermocline+0.5, *fittedParameters)
        
        pz_o = self.xData[self.find_nearest(T_fitted_profile, Tz_o)]
        pz_u = self.xData[self.find_nearest(T_fitted_profile, Tz_u)]
        
        """
        fig = plt.figure(figsize=(7,8))
        plt.clf() 
        axes1 = fig.add_subplot(111)
        axes1.axhline(y = pz_o, label = "strongest gradient", color = "blue")
        axes1.axhline(y = pz_u, label = "strongest gradient", color = "blue")
        axes1.plot(T_fitted_profile, self.xData, label = "Temp °C", color = "black")
        """
        
        #plt.gca().invert_yaxis()
        
        #p_diff=pz_o-pz_u;

        #p0 = numpy.mean(R)
        #BV=(-(9.82/p0)*(p_diff/1))**0.5;
        #N2=BV**2;

        #if te<4 then BV_korr=0;
        #if te>=4 and te>=th then BV_korr=BV*1;
        #if te>4 and te<th then BV_korr=0;
        #if BV_korr<0.05 then ztherm=110; *0.06;
        #if BV_korr>=0.05 then ztherm=zthermocline; *0.06;
        #T_zmix=Th+(Te-Th)*((1/(1+((a_temp*zmix)**n_temp)))**(1-(1/n_temp)));



    
        return T_fitted_profile, zthermocline
    
    
    # /*3.- Model the TEMP vertical profile by non-linear fitting*/
    def func1(self, x, a_temp, n_temp):
        
        return self.th+(self.te-self.th)*((1/(1+((a_temp*x)**n_temp)))**(1-(1/n_temp)))
    
    def func2(self, x, A, B, C, D): #logistic4
        """4PL lgoistic equation."""
        return ((A-D)/(1.0+((x/C)**B))) + D
    
    # function for genetic algorithm to minimize (sum of squared error)
    def sumOfSquaredError(self, parameterTuple):
        warnings.filterwarnings("ignore") # do not print warnings by genetic algorithm
        val = self.func1(self.xData, *parameterTuple)
        return numpy.sum((self.yData - val) ** 2.0)
    
    
    def generate_Initial_Parameters(self):
        # min and max used for bounds
        #maxX = max(yData)
        #minX = min(yData)
        #maxY = max(yData)
        #minY = min(yData)
    
        parameterBounds = []
        parameterBounds.append([0.01, 30.0]) # seach bounds for a_temp
        parameterBounds.append([0.1, 30.0]) # seach bounds for n_temp
        #parameterBounds.append([0.0, maxY]) # seach bounds for Offset
    
        # "seed" the numpy random number generator for repeatable results
        result = differential_evolution(self.sumOfSquaredError, parameterBounds, seed=3)
        return result.x




    def find_nearest(self, array, value):
        array = numpy.asarray(array)
        idx = (numpy.abs(array - value)).argmin()
        return idx









