# -*- coding: utf-8 -*-
"""
@author: Anders Nielsen
"""
"""
# This is as trnaslated and slightly modified version of a R-script from
# Tom Shatwell. He has the following notes about the concept:

# this function estimates the mixed depth. It finds the minimum curvature in the
# profile (maximum for winter stratification) as the border of the surface mixed
# layer. Then it finds the thermocline as the depth of maximum T-gradient. It 
# estimates the mixed layer depth as the depth where the regression line through
# the surface layer temperatures intersects with the regression line through the
# thermocline temperatures. 

# It performs some other checks, like whether stratification exists 
# (Ts-Tb) > threshold min.dT, and whether the thermocline is above the surface 
# layer, or whether the lake is completely isothermal and there is no intersection 
# (and returns NA). If mixed, it assumes the mixed layer depth is the maximum 
# depth. It also plots the profile if desired (plot=TRUE), marking the surface 
# layer and inflection point in red and the thermocline in blue. 
# min.gradient is not used currently.

###############################################################################
# Other notes about detecting the thermocline etc.:
# The slope of the curve (i.e. dT/dh) is at maximum at the obvious thermocline.
# Furthermore, because the curvature of the curve changes at the thermocline, 
# a point of inflection, then, by definition d2T/dx2 = 0.

# Another way to look at it is that to maximise the slope, i.e. the 1st 
# derivative, then the 2nd derivative must be equal to zero.
"""
###############################################################################

"""
# Scripting below is based on data from GOTM by Prisca
"""

import netCDF4
import numpy as np
import datetime
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy, scipy, matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.optimize import differential_evolution
import warnings
import copy
from collections import defaultdict
import pandas as pd

import sys
sys.path.append('O:/Tech_DYAMO-DK/07_CODE/Thermocline')

# script specific functions
import fitting_profile as cf
import thermocline_utils as t_utils
import schmidt_stability as st



# specifying dataset
datasetNC = "O:/Tech_DYAMO-DK/07_CODE/Thermocline/DATA/Bryrup/output.nc"
#datasetNC = "C:/Users/au595054/Desktop/lake_bryrup_denmark_check/output.nc"
datasetOBS = "O:/Tech_DYAMO-DK/07_CODE/Thermocline/DATA/obs/TEMP.obs"
datasetHYPS = "O:/Tech_DYAMO-DK/07_CODE/Thermocline/DATA/obs/hypsograph.dat"

datasetOBS = "O:/Tech_DYAMO-DK/07_CODE/Thermocline/DATA/Relab/tempv.obs"
datasetHYPS = "O:/Tech_DYAMO-DK/07_CODE/Thermocline/DATA/Relab/hypsograph.dat"


"""
UNUSED !
# in case the lake to analyse is very deep you can limit the dept to analyse for gradients:
analysis_max_depth = -250.0
# the period to analyse. NB! these are unused at the moment
analysis_start_date = datetime.datetime(2046, 1, 1, 12, 0, 0)  #"2046-01-02 12:00:00"
analysis_end_date = datetime.datetime(2051, 1, 1, 12, 0, 0) 
"""

# in case the data you have is high frequency, midday heatings in the surface layers,
# may create temporary double thermoclines. You can specify a "hmix_min" being
# a minimum depth to search for the thermocline (basically pushing the calculations
# downwards)
hmix_min = 10.0

# narrowing threshold for the thermocline regression fit. The higher the narrower
# depth-interval to identify the points representing the thermocline.  
therm_dz = 3

# defining the number of layers to include in the regression fit of the surface slope.
surfRegShiftLayers = 3

# threshold of the given variable (either temperature og density) used to evaulate
# if the difference between top and bottom are larger than this threshold. 
dVAR_min = 0.4
# threshold of the given variable (either temperature og density) used to evaulate
# if the standard-diviation throughout the profile are larger than this threshold.
# if not the lake is considered fully mixed.
T_std_threshold = 0.1

#flag to activate profile fitting
try_use_fitted_profile = False

#thermocline_z_min = 15.0
i_2_z_min = 0.5 #minimum curvature
#i_3_z_min = 27.0 #maximum curvature
interp_step = 0.5

# threshold defining the minimum required profile depths from the .obs files
profileThreshold = 4

# choose the type of variable to use.
variable_to_use = "T" # temperature
#variable_to_use = "R" # density

datasetType = "obs"
results_B = run_it(datasetOBS)

datasetType = "model"
results_C = run_it(datasetNC, variable_to_use)


i = 5 #4 #160 #194 #203
#plotting(results_B, i)

import thermocline_plotting as t_plot
t_plot.plotly_individual(results_C, i)        

t_plot.plotly_temporal(results_C, var = "stability") # var = "mix" or "stability"


#plt.plot(results_B[i]["R_org"],results_B[i]["z_org"])
#plt.show()

#dataset = datasetOBS
#dataset = datasetNC

def run_it(dataset, var_to_use = "T"):

    if datasetType == "model":
        # import from GOTM model
        with netCDF4.Dataset(dataset) as nc:
            
            z = nc.variables["z"][..., 0, 0]
            h = nc.variables["h"][..., 0, 0]
            temp = nc.variables["temp"][..., 0, 0]
            density = nc.variables["rho"][..., 0, 0]
            sal = nc.variables["salt"][..., 0, 0] # g/kg
            bthA = nc.variables["Af"][..., 0, 0] # m2, hypsograph at grid interface
            depths = z * -1
            #bthD = depths # m, bathymetry depths is identical to z in model
            mixed_bot = nc.variables["mld_bott"][..., 0, 0] # bottom mixed layer depth (m)
            mixed_surf = nc.variables["mld_surf"][..., 0, 0] # surface mixed layer depth (m)
            # TODO: compare mixed_bot with z_mix
            #surf_fric = nc.variables["u_taus"][..., 0, 0] # surface water friction (due to wind?) (m/s)            
            
            nctime = nc.variables['time']
            time_vals = nctime[:]
            time_unit = nctime.units
            datetime_fmt = (netCDF4.num2date(time_vals, time_unit))         
    
    elif datasetType == "obs":
        # import OBS profiles and hypsograph data
        try:
            obsData = t_utils.read_obs_format(dataset)
            
            hypsDepth, hypsArea = t_utils.ReadHypsograph(datasetHYPS)
            hypsDepth = hypsDepth[-1]-hypsDepth
        
        except Exception as e:
            print("The format of the .obs file is not correct")
            print(e)
            
        datetime_fmt        = obsData["obsTime"]
        zImport             = obsData["obsDepth"]
        tempImport          = obsData["obsValue"]
        densityCalculated   = t_utils.water_density(tempImport)
        
        # removin profiles with too few observations:
        zImport = pd.DataFrame(zImport)
        Bool = zImport.dropna(thresh=profileThreshold).index.values.tolist()
                             
        z = zImport.iloc[Bool].to_numpy()
        
        
        tempImport = pd.DataFrame(tempImport)
        temp = tempImport.iloc[Bool].to_numpy()
        
        densityCalculated = pd.DataFrame(densityCalculated)
        density = densityCalculated.iloc[Bool].to_numpy()
        
        datetime_fmt = pd.DataFrame(datetime_fmt)
        datetime_fmt = datetime_fmt.iloc[Bool].to_numpy()
                
            
    #depth_cutoff = find_nearest(z, analysis_max_depth)
    #date_mask = (datetime_fmt >= analysis_start_date) & (datetime_fmt <= analysis_end_date )
    
    
 
        
    #what about warmup period
    
    # creating dict to hold the results produced
    results = {}
    for i, d in enumerate(datetime_fmt): # datetime_fmt[date_mask]
        results[i] =    {    
                        "datasetType"                       : datasetType,
                        "date"                              : d,            #d.strftime('%Y-%m-%d'),
                        "note"                              : None,
                        "stratification"                    : None,         # "yes" / "no"
                        "type"                              : None,         # "winter"/ "summer"
                        "profile_fitting_possible"          : None,
                        "data_for_analysis"                 : None,
                        "plot"                              : None,
                        "z_mix"                             : None,
                        "z_epi_bottom"                      : None,
                        "z_hyp_top"                         : None,
                        "strongest gradient (i1) z"         : None,
                        "minimum curvature (i2) z"          : None,
                        "maximum curvature (i3) z"          : None,
                        "variable_to_use"                   : var_to_use,
                        "i_1"                               : None,
                        "i_2"                               : None,
                        "i_3"                               : None,
                        "coef"                              : None,
                        "T_obs"                             : None,
                        "T_obs_label"                       : "Temp (°C) - obs",
                        "R_obs"                             : None,
                        "R_obs_label"                       : "Density (kg/m3) - obs",
                        "z_obs"                             : None,
                        "z_obs_label"                       : "Depth (m)",
                        "T_org"                             : None,
                        "T_org_label"                       : "Temp (°C) - original",
                        "R_org"                             : None,
                        "R_org_label"                       : "Density (kg/m3) - original",
                        "z_org"                             : None,
                        "z_org_label"                       : "Depth (m)",
                        "T_fitted"                          : None,
                        "T_fitted_label"                    : "Temp (°C) - fitted",
                        "R_fitted"                          : None,
                        "R_fitted_label"                    : "Density (kg/m3) - fitted",
                        "z_fitted"                          : None,
                        "z_fitted_label"                    : "Depth (m)",
                        "valid_thermocline"                 : None,
                        "St"                                : None,
                        "mixed_layer_depth"                 : None,
                        "curvature"                         : None
                        }
    
       
    #for i, (T_raw, R_raw, z_raw, depths_raw, sal_raw, bthA_raw) in enumerate(zip(temp, density, z, depths, sal, bthA)):    
    for i, (T_raw, R_raw, z_raw) in enumerate(zip(temp, density, z)):    
    
        print("==============================================")
        print("ID: %s" %i)
        print("==============================================")
        
        # the raw datasets are saved in the dict for later plotting. this is
        # only relevant for obs imports to ensure same length
        if results[i]["datasetType"] == "obs":
           results[i]["T_obs"]: T_raw
           results[i]["R_obs"]: R_raw
           results[i]["z_obs"]: z_raw
        
        # deleting possible nan values.
        T_raw = T_raw[~np.isnan(T_raw)]
        z_raw = z_raw[~np.isnan(z_raw)]
        R_raw = R_raw[~np.isnan(R_raw)]
               
        # create interpolated profiles:
        interpolateProfiles(i, T_raw, R_raw, z_raw, interp_step, results)
        
        #basic check of the temperature data for each day to see if variations exists vertically    
        T_mean = np.mean(T_raw)
        T_std = np.std(T_raw)
        print("temperature standard deviation is %s: " %T_std)
        
        if T_std < T_std_threshold:
            print("fully mixed water column")
            # updating results:
            results[i]["stratification"] = "no"
            results[i]["note"] = "was classified from the standard deviation of temperature"
            #pass
        else:
            print("continue calculations")
            # estimate water column zones
            analyze_profiles(i, results)
            print("==============================================")
        
        if results[i]["datasetType"] == "obs":
            # use these: hypsDepth, hypsArea
            results[i]["St"] = st.schmidt_stability(T_raw, z_raw, hypsArea, hypsDepth, sal = 0, rhoL_in = R_raw)[0]
            print("Schmidt stability calculated.")
        
        
        
        if results[i]["datasetType"] == "model":
            # extract mixed layer depth from model
            results[i]["mixed_layer_depth"] = mixed_surf[i] # mixed_bot[i]
        
            # calculate stablity
            depths_raw = depths[i]
            sal_raw = sal[i]
            bthA_raw = bthA[i]
            results[i]["St"] = st.schmidt_stability(T_raw, depths_raw, bthA_raw, depths_raw, sal = sal_raw, rhoL_in = R_raw)[0]
            print("Schmidt stability calculated.")

    return results


def interpolateProfiles(i, T_raw, R_raw, z_raw, interp_step, results):
    # profiles are flipped around and the depth is corrected to positive
    z_in = np.flip(z_raw, axis = 0) * -1.0
    T_in = np.flip(T_raw, axis = 0)
    R_in = np.flip(R_raw, axis = 0)   
    
    # interpolate to evenly distributed depth intervals for high resolution 
    # vertical data (0.1m)
    z_high_int = numpy.arange(0.1,numpy.max(z_in),interp_step)
    # interpolation:
    T_high_int = np.interp(z_high_int,z_in,T_in)
    R_high_int = np.interp(z_high_int,z_in,R_in)
    
    # the original interpolated data is pushed to the dict:
    results[i]["T_org"] = T_high_int
    results[i]["R_org"] = R_high_int
    results[i]["z_org"] = z_high_int
    
    if try_use_fitted_profile == True:
    
        try:
            T_fitted_profile , zthermocline = cf.curve_fitting().fitting_curve_to_profile(T_high_int,z_high_int)
            results[i]["profile_fitting_possible"] = True
        except:
            # in the case that the fit is not possible then skip
            results[i]["profile_fitting_possible"] = False
        
    
    if try_use_fitted_profile == True and results[i]["profile_fitting_possible"] == True :
        R_from_fitted_T_profile = t_utils.water_density(T_fitted_profile,sal=0.15)
        
        results[i]["data_for_analysis"] = "profile_fitted"
        
        
        # define the data used for the comming calculations
        T = T_fitted_profile
        R = R_from_fitted_T_profile
        z = z_high_int
        
        results[i]["T_fitted"] = T
        results[i]["R_fitted"] = R
        results[i]["z_fitted"] = z
    
    else:
        # define the data used for the comming calculations
        # the interpolated profile data is used
        results[i]["data_for_analysis"] = "profile_original"
    
    
    return

    

def analyze_profiles(i, results):
        
       
    if results[i]["data_for_analysis"] == "profile_fitted":
        
        # define the data used for the comming calculations
        T = results[i]["T_fitted"]
        R = results[i]["R_fitted"]
        z = results[i]["z_fitted"]
                
    else:
        # define the data used for the comming calculations
        # the interpolated profile data is used
        results[i]["data_for_analysis"] = "profile_original"
        
        T = results[i]["T_org"]
        R = results[i]["R_org"]
        z = results[i]["z_org"]
    
    
    # get the characteristics of the curve:
    curT, curR, dTdz, dRdz = get_gradient_and_curvature(T,R, z, method =2)
    
    results[i]["curvature"] = {"curT" : curT, 
                               "curR" : curR, 
                               "dTdz" : dTdz, 
                               "dRdz" : dRdz
                               }
    
    #plt.plot(dTdz, z)
    
    if variable_to_use == "T":
    
        dVARdz = dTdz
        curVAR = curT
        VAR = T
        
    elif variable_to_use == "R":
    
        dVARdz = dRdz
        curVAR = curR
        VAR = R
           
    
    i_1 = np.argmin(dVARdz) # index of strongest gradient (-ve for positive strat, +ve for winter strat)
    i_2 = np.argmin(curVAR) # index of minimum curvature (bot epilimnion)
    i_3 = np.argmax(curVAR) # index of maximum curvature (top hypolimnion)
    
    print("strongest gradient is in depth: %s" %z[i_1])
    print("minimum curvature (i_2) is in depth: %s" %z[i_2])
    print("maximum curvature (i_3) is in depth: %s" %z[i_3])
    
    if z[i_1] <= z[i_2] and z[i_3] <= z[i_2]: 
        results[i]["note"] = "both i1 and i3 are lower than i2."
       
    if z[i_2] <= hmix_min:
        print("the mixed layer is closer to the surface than the threshold specified")
        
        print("finding new parameters...")
        """
        valid = np.array([hmix_min <= zi for zi in z], dtype=bool)
        
        i_1 = np.argmin(dVARdz[valid]) # index of strongest gradient (-ve for positive strat, +ve for winter strat)
        i_2 = np.argmin(curVAR[valid]) # index of minimum curvature (bot epilimnion)
        i_3 = np.argmax(curVAR[valid]) # index of maximum curvature (top hypolimnion)
        z_k = z[valid]
        
        print("strongest gradient is in depth: %s" %z[i_1])
        print("minimum curvature (i_2) is in depth: %s" %z[i_2])
        print("maximum curvature (i_3) is in depth: %s" %z[i_3])
        
        z_kk = z_k[i_1]
        i_1 = find_nearest(z, z_kk)
        
        z_kk = z_k[i_2]
        i_2 = find_nearest(z, z_kk)
        
        z_kk = z_k[i_3]
        i_3 = find_nearest(z, z_kk)
        """
    
    if z[i_2] <= i_2_z_min:
        
        valid = np.array([i_2_z_min <= zi for zi in z], dtype=bool)
        z_k = z[valid]
        i_2 = np.argmin(curVAR[valid])
        z_kk = z_k[i_2]
        i_2 = find_nearest(z, z_kk)
        
        print("new minimum curvature (i_2) depth:")
        print(z[i_2])
        
    if z[i_1] <= z[i_2]:
        # BUG: this cond. sometimes create false new gradient e.g i=194
        valid = np.array([z[i_2] <= zi for zi in z], dtype=bool)
        z_k = z[valid]
        i_1 = np.argmin(dVARdz[valid])
        z_kk = z_k[i_1]
        i_1 = find_nearest(z, z_kk)
        
        #results[i]["note"] = "identified new strongest gradient."
        print("new strongest gradient (i_1) depth:")
        print(z[i_1])
    
    
    
    if z[i_3] <= z[i_2]:
                       
        valid = np.array([z[i_2] <= zi for zi in z], dtype=bool)
        z_k = z[valid]
        i_3 = np.argmax(curVAR[valid])
        z_kk = z_k[i_3]
        i_3 = find_nearest(z, z_kk)
        
        print("new maximum curvature (i_3) depth:")
        print(z[i_3])
    
    if z[i_2] == z[i_3]:
        results[i]["stratification"] = "no"
        results[i]["note"] = "i2 and i3 are identical."
    
    
    if i_1 <= i_2:
        print("testing while...")
        #print("the mixed layer is closer to the surface than the threshold specified")
        valid = np.array([z[i_1] <= zi for zi in z], dtype=bool)
        i_1 = np.argmin(dVARdz[valid])
        z_kk = z_k[i_1]
        i_1 = find_nearest(z, z_kk)
        print("i_1: %s" %i_1)
        
    
    if(min(z)<2):
        valid = np.array([2 >= zi for zi in z], dtype=bool)
        VAR_surface = VAR[valid]
        VAR_surface_mean = np.mean(VAR_surface)
    else:
        VARs = np.mean(VAR[0:i_1])
        print("Warning: Surface temp estimated as temps above greatest curvature")
        
    valid_bot = np.array([zi>=(max(z)-2) for zi in z], dtype=bool)
    
    VAR_bottom = VAR[valid_bot]
    z_bottom = z[valid_bot]
        
    VAR_bottom_mean = np.mean(VAR_bottom)
    
    
    # assume mixed to max(depths) if Ts-Tb < min.dT
    if(abs(VAR_surface_mean-VAR_bottom_mean) < dVAR_min):
        print("the lake is fully mixed")
        
        results[i]["stratification"] = "no"
        results[i]["note"] = "was classified from validating difference between top and bottom against threshold"
        
    if results[i]["stratification"] == "no":
        
        return
    
    else:
        # continue calculations:
        if variable_to_use == "T":
            
            # in case of winter temperatures:
            if (VAR_surface_mean < 4):
                i_1 = np.argmax(dVARdz) # index of maximum gradient
                i_2 = np.argmax(curVAR) # max curvature for winter stratification
                i_3 = np.argmin(curVAR) # min curvature for winter stratification
                print("the lake temperature is less than 4 degress")
                print("checking for winter stratification")
                print("strongest gradient is in depth: %s" %z[i_1])
                print("minimum curvature (i_2) is in depth: %s" %z[i_2])
                print("maximum curvature (i_3) is in depth: %s" %z[i_3])
                
                results[i]["type"] = "winter"
                results[i]["stratification"] = "yes"
            else:
                results[i]["type"] = "summer"
                results[i]["stratification"] = "yes"
            
            # check if number of thermocline layers are adequate
            therm_upper = i_2+therm_dz
            if therm_upper >= len(z):
                therm_upper = i_2
            valid_thermocline = np.array([z[i_3-therm_dz] >= zi >= z[therm_upper] for zi in z], dtype=bool)
            
            # handle if i_3 is deepest layer
            therm_bottom = i_3+1
            if therm_bottom >= len(z):
                therm_bottom = i_3
            if sum(valid_thermocline) < 2:
                valid_thermocline = np.array([z[therm_bottom] >= zi >= z[i_2-1] for zi in z], dtype=bool)
            

        #h1 = np.mean(z[i_1:i_1+2]) # depth of maximum gradient
        #h2 = z[i_2+1] # depth of inflection    
        
        
        # thermocline:       
        #valid_thermocline = np.array([z[i_3]-therm_dz >= zi >= z[i_2]+therm_dz for zi in z], dtype=bool)        
        
        # if less than 2 in length then don't do regression
        if sum(valid_thermocline) < 2:
            results[i]["note"] = "thermocline has less than 2 depth point. Regression is not possible"
               
        else:           
            VAR_thermocline = VAR[valid_thermocline]
            z_thermocline = z[valid_thermocline]
            
            coef = []
                        
            surfRegShift = i_2 - surfRegShiftLayers
            
            if surfRegShift < 0:
                surfRegShift = 0
            
            #if z[i_2] <= hmix_min:
            #    print("the mixed layer is closer to the surface than the threshold specified")
            #    results[i]["stratification"] = "no"
            #    results[i]["note"] = "the mixed layer is closer to the surface than the threshold specified"
            
            if results[i]["stratification"] != "no":
                i_2_sel = i_2
                if len(VAR[surfRegShift:i_2]) == 0:
                    i_2_sel = i_2 + 2
                if len(VAR[surfRegShift:i_2]) == 1:
                    i_2_sel = i_2 + 1
                if i_2 != i_2_sel:
                    print("i2 corrected due to too shallow epilimnion.")
                    
                surf_SLOPE, surf_INTER      = linear_regression(VAR[surfRegShift:i_2_sel],z[surfRegShift:i_2_sel])
                #surf_SLOPE, surf_INTER      = linear_regression(VAR[0:i_2],z[0:i_2])
                
                thermo_SLOPE, thermo_INTER  = linear_regression(VAR_thermocline,z_thermocline)
                bot_SLOPE, bot_INTER        = linear_regression(VAR_bottom,z_bottom)
                print("regression calculated...")
                        
                coef.append(surf_SLOPE), coef.append(surf_INTER), coef.append(thermo_SLOPE), coef.append(thermo_INTER), coef.append(bot_SLOPE), coef.append(bot_INTER)
                
                results[i]["coef"] = coef
                
                
                # check if the regression of the surface was succesfull
                #if(is.na (surf_SLOPE) surf_SLOPE = 0
                
                # depth of intersection of thermocline and surface layer regression lines
                z_top_intersect = get_intersection(surf_SLOPE,surf_INTER,thermo_SLOPE,thermo_INTER)
                z_bottom_intersect = get_intersection(thermo_SLOPE,thermo_INTER,bot_SLOPE,bot_INTER)
                
                try:
                    z_top_intersect = round(z_top_intersect,1)
                except TypeError:
                    pass
                
                try:
                    z_bottom_intersect = round(z_bottom_intersect, 1)
                except TypeError:
                    pass
                
                
                # ignore if intersection of thermocline and surface layer is below thermocline
                try:
                    interValidator = z[i_1 + 1]
                except IndexError:
                    interValidator = z[i_1]
                
                if z_top_intersect == None or z_top_intersect > interValidator:
                    z_top_intersect == None
                # ignore if minimum curvature is below the thermocline
                if(i_1 < i_2):
                    z_top_intersect = None
                # assume mixed to max(depths) if Ts-Tb < min.dT
                if(abs(VAR_surface_mean-VAR_bottom_mean) < dVAR_min):
                    z_top_intersect = max(z) 
                
                
                results[i]["z_mix"] = z_top_intersect
                results[i]["z_epi_bottom"] = z_top_intersect
                results[i]["z_hyp_top"] = z_bottom_intersect
                        
        results[i]["strongest gradient (i1) z"]    = z[i_1]
        results[i]["minimum curvature (i2) z"]     = z[i_2]
        results[i]["maximum curvature (i3) z"]     = z[i_3]
        
        results[i]["i_1"]     = i_1
        results[i]["i_2"]     = i_2
        results[i]["i_3"]     = i_3
        
        results[i]["valid_thermocline"] = valid_thermocline    
            
    
        return

#=====================================================================================================================
# plotting
#=====================================================================================================================

def plotting(results, i):
    
    Rs = results[i]
    
    fig = plt.figure(figsize=(7,8))
    plt.clf()
    fig.suptitle("id: %s date: %s stratification: %s" %(i, results[i]["date"], results[i]["stratification"]), fontsize=12)
    
    axes1 = fig.add_subplot(111)    
    axes1.set_title("reason: %s" %(results[i]["note"]), fontsize=10)
    
    if Rs["data_for_analysis"] == "profile_original":
        
        if variable_to_use == "T":
            VAR = "T_org"
            var_label = "Temp °C - original"
            
        elif variable_to_use == "R":
            VAR = "R_org"
            var_label = "Density - original"
        
        z_var = "z_org"
        
        axes1.plot(Rs[VAR],Rs[z_var], label = var_label, color = "black")
        
    elif Rs["data_for_analysis"] == "profile_fitted":
    
        if variable_to_use == "T":
            VAR = "T_fitted"
            VAR2 = "T_org" 
            var_label = "Temp °C - fitted"
            var2_label = "Temp °C - original"
            
        elif variable_to_use == "R":
            VAR = "R_fitted"
            VAR2 = "R_org"
            var_label = "Density - fitted"
            var2_label = "Density - original"
        
        z_var = "z_fitted"
        
              
        axes1.plot(Rs[VAR],Rs[z_var], label = var_label, color = "black")
        axes1.plot(Rs[VAR2],Rs["z_org"], label = var2_label, color = "grey")
    
    axes1.xaxis.set_label_position('bottom')
    axes1.set_xlabel(var_label, color = "black")
    axes1.set_ylabel("Depth (m)", color = "black")
    
    #axes1.axhline(y = z[i_1], label = "strongest gradient", color = "blue")
    #axes1.axhline(y = z[i_2], label = "minimum curvature (bot epilimnion)", color = "red")
    #axes1.axhline(y = z[i_3], label = "maximum curvature (top hypolimnion)", color = "green")
    
    if Rs["stratification"] == "no":
        
        print("skipping thermocline")
    
    else:
        try:
            axes1.scatter(Rs[VAR][Rs["i_1"]],Rs[z_var][Rs["i_1"]], label ="i_1")
            axes1.scatter(Rs[VAR][Rs["i_2"]],Rs[z_var][Rs["i_2"]], label ="i_2")
            axes1.scatter(Rs[VAR][Rs["i_3"]],Rs[z_var][Rs["i_3"]], label ="i_3")
            
            axes1.plot(Rs[VAR][Rs["valid_thermocline"]],Rs[z_var][Rs["valid_thermocline"]], label ="thermocline")
        except:
            print("no thermocline")
        try:
            axes1.axhline(y = Rs["z_epi_bottom"], label = "thermocline top", color = "red")
            axes1.axhline(y = Rs["z_hyp_top"], label = "thermocline bottom", color = "blue")
        except:
            print("no thermocline")
            
        try:
            surf_SLOPE, surf_INTER, thermo_SLOPE, thermo_INTER, bot_SLOPE, bot_INTER = Rs["coef"]
        
            abline(surf_SLOPE, surf_INTER)
            abline(thermo_SLOPE, thermo_INTER)
            abline(bot_SLOPE, bot_INTER)
        
        except:
            print("no regression")
        #plt.plot(temp_in, reg_surf_intercept + reg_surf_slope*temp_in, 'r', label='fitted line')
        
    axes1.set_xlim(min(Rs[VAR])*0.9, max(Rs[VAR])*1.1)
    axes1.set_ylim(min(Rs[z_var])*0.9, max(Rs[z_var])*1.1)
    
    
    ## fitted profile
    # create data for the fitted equation plot
    #xModel = numpy.linspace(min(xData), max(xData))
    #yModel = func1(xModel, *fittedParameters)
    
    # now the model as a line plot
    #axes1.plot(T_fitted_profile,z)
    #axes1.axhline(y = zthermocline, label = "zthermocline", color = "orange")
    
    plt.legend()
    
    plt.gca().invert_yaxis()
    
    """
    axes3 = axes1.twiny()
    axes3.scatter(cur,depth_in, label = "ODO mg/L", color = "black")
    axes3.xaxis.set_label_position('top')
    axes3.set_xlabel("DO mg/l", color = "blue")
    """
    
    return axes1


#=============================================================================#

def nth_val(a, method = "sml"):
    
    if method == "sml":
        n = 2
        return np.argpartition(a, n)[n]
    if method == "lrg":
        return np.argpartition(a, -2)[-2]
    


def linear_regression(x,y):
    # if we have a small or zero standard variation in the temperature we need
    # to handle the regression of a near vertical line:
    
    # x => input to the function is the variable (e.g. temperature og density)
    # y => input to the function is the depth z
    
    std_x = np.std(x)
    
    if std_x < 0.05:
        print("the temperature has very little variation for regression. the mean of the temperature is calulated as proxy for the regression fit") # SKAL INDSÆTTES SOM KOMMENTAR I RESULTS DICT
        slope       =  "vertical line"
        intercept   = numpy.mean(x)

    # else the normal regression:
    else:
        fit = numpy.polyfit(x,y,1) 
    
        slope =  fit[0]
        intercept = fit[1]
        #slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
    
    return slope, intercept


def get_intersection(slope1,intersect1,slope2,intersect2):
        
    if slope1 == slope2:
        print ("Lines are parallel!!!")
        return None
    
    elif slope1 == "vertical line":
        x_intersect_value = intersect1
        y_intersect_value = slope2*intersect1 + intersect2
        
    elif slope2 == "vertical line":
        x_intersect_value = intersect2
        y_intersect_value = slope1*intersect2 + intersect1
    
    else:
        x_intersect_value = (intersect2 - intersect1) / (slope1 - slope2)
        y_intersect_value = slope1 * x_intersect_value + intersect1
    return y_intersect_value




def abline(slope, intercept):
    """Plot a line from slope and intercept or vertical lines"""
    axes = plt.gca()
    
    if slope == "vertical line":
        y_vals = np.array(axes.get_ylim())
        
        x_vals = np.empty(len(y_vals))
        x_vals.fill(intercept)
        
    else:
        x_vals = np.array(axes.get_xlim())
        y_vals = intercept + slope * x_vals
    print(x_vals)
    print(y_vals)    
    plt.plot(x_vals, y_vals, '--')
    
    return


def find_nearest(array, value):
    array = numpy.asarray(array)
    idx = (numpy.abs(array - value)).argmin()
    return idx



# calculating curve details:
def get_gradient_and_curvature(T, R, z, method = 2):

    if method == 1:

        dTdz = np.diff(T) / np.diff(z)
        dRdz = np.diff(R) / np.diff(z)
        
        curT = np.diff(np.diff(T)) / np.diff(z,n=2)**2
        
        curR = np.diff(np.diff(R)) / np.diff(z,n=2)**2

    if method == 2:
        # https://stackoverflow.com/questions/50604298/numerical-calculation-of-curvature
        # first derivatives
        dz = np.gradient(z)
        dTdz = np.gradient(T) / dz
        
        dRdz = np.gradient(R) / dz

        # second derivatives
        d2z = np.gradient(dz)  
        d2T = np.gradient(dTdz)
        d2R = np.gradient(dRdz)
        
        # curvature
        curT = (d2T) / (np.sqrt(1 + dTdz ** 2)) ** 1.5
        curR = (d2R) / (np.sqrt(1 + dRdz ** 2)) ** 1.5
        #d2T_dz2 =  d2T / dz**2

    return curT, curR, dTdz, dRdz








"""

 # first derivatives
dz = np.gradient(z, z)
dT = np.gradient(T, z)

# second derivatives
d2z = np.gradient(dz, z)  
d2T = np.gradient(dT, z)

T_cur = (d2T) / (np.sqrt(1 + dT ** 2)) ** 1.5  # curvature

d2T_dz2 =  d2T / dz**2


i_1 = np.argmin(dT) # index of strongest gradient (-ve for positive strat, +ve for winter strat)
i_2 = np.argmin(d2T_dz2) # index of minimum curvature (bot epilimnion)
i_3 = np.argmax(d2T_dz2) # index of maximum curvature (top hypolimnion)


dx = np.gradient(depth_in)
dy = np.gradient(temp_in)

d2x = np.gradient(dx)
d2y = np.gradient(dy)

curvature = abs(dx * d2y - d2x * dy) / (dx * dx + dy * dy) ** 1.5
"""


