# -*- coding: utf-8 -*-
"""
Created on Sat May 11 13:34:34 2019

@author: au302246
"""
"""

it is easily see that the slope of the curve (i.e. dT/dh) is a maximum at the (very!) obvious thermocline.
Furthermore, because the curvature of the curve changes at the thermocline, a point of inflection, then, by definition d2T/dx2 = 0.
Another way to look at it is that to maximise the slope, i.e. the 1st derivative, then the 2nd derivative must be equal to zero.
"""


import netCDF4
import numpy as np
import datetime
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy, scipy, matplotlib
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.optimize import differential_evolution
import warnings

# script specific functions
import fitting_profile as cf
import thermocline_utils as t_utils

# specifying dataset
dataset = "C:/temp/output.nc"

hmix_min = 3.0
therm_dz = 0.5
dT_min = 1.0
T_std_threshold = 1.0
use_fitted_profile = False


with netCDF4.Dataset(dataset) as nc:
    
    z = nc.variables["z"][..., 0, 0]
    h = nc.variables["h"][..., 0, 0]
    temp = nc.variables["temp"][..., 0, 0]
    density = nc.variables["rho"][..., 0, 0]
    
    nctime = nc.variables['time']
    time_vals = nctime[:]
    time_unit = nctime.units
    datetime_fmt = (netCDF4.num2date(time_vals, time_unit))
    
#what about warmup period

# creating dict to hold the results produced
results = {}
for i, d in enumerate(datetime_fmt[:210]):
    results[i] =    {    
                    "date": d.strftime('%Y-%m-%d'),
                    "note": None,
                    "stratification" : None,    # "yes" / "no"
                    "type": None,               # "winter"/ "summer"
                    "profile_fittiing": None,
                    "plot": None,
                    "z_mix": None,
                    "z_epi_bottom": None,
                    "z_hyp_top": None,
                    "strongest gradient (i1)": None,
                    "minimum curvature(i2)": None,
                    "maximum curvature(i3)": None,
                    "coef": None,
                    "T": None,
                    "z": None
                    }



for i, (T_raw, R_raw, z_raw) in enumerate(zip(temp[:210,:], density[:210,:], z[:210,:])):
    print("==============================================")
    print("ID: %s" %i)
    print("==============================================")
    #basic check of the temperature data for each day to see if variations exists vertically    
    T_mean = np.mean(T_raw)
    T_std = np.std(T_raw)
    
    if T_std < T_std_threshold:
        print("fully mixed water column")
        # updating results:
        results[i]["stratification"] = "no"
        results[i]["note"] = "was classified from the standard deviation of temperature"
        #pass
    else:
        print("continue calculations")
        # estimate water column zones
        analyze_profiles(i, T_raw, R_raw, z_raw)
        print("==============================================")

    

def analyze_profiles(i, T_raw, R_raw, z_raw):
    # profiles are flipped around and the depth is corrected to positive
    z_in = np.flip(z_raw, axis = 0) * -1.0
    T_in = np.flip(T_raw, axis = 0)
    R_in = np.flip(R_raw, axis = 0)   
    
    # interpolate to evenly distributed depth intervals for high resolution 
    # vertical data (0.1m)
    z_high_int = numpy.arange(0.1,numpy.max(z_in),0.1)
    # interpolation:
    T_high_int = np.interp(z_high_int,z_in,T_in)
    R_high_int = np.interp(z_high_int,z_in,R_in)
    
    #curve_fit:
    try:
        T_fitted_profile , zthermocline = cf.curve_fitting().fitting_curve_to_profile(T_high_int,z_high_int)
        results[i]["profile_fittiing"] = True
    except:
        # in the case that the fit is not possible then skip
        results[i]["profile_fittiing"] = False
    
    
    if use_fitted_profile == True and results[i]["profile_fittiing"] == True :
        R_from_fitted_T_profile = t_utils.water_density(T_fitted_profile,sal=0.15)
        
        # define the data used for the comming calculations
        T = T_fitted_profile
        R = R_from_fitted_T_profile
        z = z_high_int
    
    else:
        # define the data used for the comming calculations
        # the interpolated profile data is used
        T = T_high_int
        R = R_high_int
        z = z_high_int
    
    
    # get the characteristics of the curve:
    curT, dTdz, dRdz = get_gradient_and_curvature(T,R, z, method =2)
    
    #plt.plot(curT, z)
    
    i_1 = np.argmin(dTdz) # index of strongest gradient (-ve for positive strat, +ve for winter strat)
    i_2 = np.argmin(curT) # index of minimum curvature (bot epilimnion)
    i_3 = np.argmax(curT) # index of maximum curvature (top hypolimnion)
    
    print("strongest gradient is in depth: %s" %z[i_1])
    print("minimum curvature (i_2) is in depth: %s" %z[i_2])
    print("maximum curvature (i_3) is in depth: %s" %z[i_3])
    
    
    if z[i_2] <= hmix_min:
        print("the mixed layer is closer to the surface than the threshold specified")
    
    if z[i_2] <= 3:
        i_2 = nth_val(curT, method = "sml")
        print("obs surface near")
        print(i_2)
    
    
    if i_1 <= i_2:
        print("the mixed layer is closer to the surface than the threshold specified")
    
    if(min(z)<2):
        valid = np.array([2 >= zi for zi in z], dtype=bool)
        T_surface = T[valid]
        T_surface_mean = np.mean(T_surface)
    else:
        Ts = np.mean(T[0:i_1])
        print("Warning: Surface temp estimated as temps above greatest curvature")
        
    valid = np.array([zi>=(max(z)-2) for zi in z], dtype=bool)
    T_bottom = T[valid]
    z_bottom = z[valid]
    T_bottom_mean = np.mean(T_bottom)
    
    
    # assume mixed to max(depths) if Ts-Tb < min.dT
    if(abs(T_surface_mean-T_bottom_mean) < dT_min):
        print("the lake is fully mixed")
    
    
    if (T_surface_mean < 4):
        i_1 = np.argmax(dTdz) # index of maximum gradient
        i_2 = np.argmax(curT) # max curvature for winter stratification
        i_3 = np.argmin(curT) # min curvature for winter stratification
        
        results[i]["type"] = "winter"
           
    else:
        results[i]["type"] = "summer"
           
        
    #h1 = np.mean(z[i_1:i_1+2]) # depth of maximum gradient
    #h2 = z[i_2+1] # depth of inflection    
    
    
    # thermocline:       
    valid = np.array([z[i_3]-therm_dz >= zi >= z[i_2]+therm_dz for zi in z], dtype=bool)        
    
    # if less than 2 in length then don't do regression
    if sum(valid) < 2:
        results[i]["note"] = "thermocline has less than 2 depth point. Regression is not possible"
           
    else:           
        T_thermocline = T[valid]
        z_thermocline = z[valid]
        
        coef = []
        
        surf_SLOPE, surf_INTER      = linear_regression(T[:i_2],z[:i_2])
        thermo_SLOPE, thermo_INTER  = linear_regression(T_thermocline,z_thermocline)
        bot_SLOPE, bot_INTER        = linear_regression(T_bottom,z_bottom)
        
        coef.append(surf_SLOPE), coef.append(surf_INTER), coef.append(thermo_SLOPE), coef.append(thermo_INTER), coef.append(bot_SLOPE), coef.append(bot_INTER)
        
        results[i]["coef"] = coef
        
        
        # check if the regression of the surface was succesfull
        #if(is.na (surf_SLOPE) surf_SLOPE = 0
        
        # depth of intersection of thermocline and surface layer regression lines
        z_top_intersect = get_intersection(surf_SLOPE,surf_INTER,thermo_SLOPE,thermo_INTER)
        z_bottom_intersect = get_intersection(thermo_SLOPE,thermo_INTER,bot_SLOPE,bot_INTER)
        
        
        # ignore if intersection of thermocline and surface layer is below thermocline
        if z_top_intersect == None or z_top_intersect > z[i_1 + 1]:
            z_top_intersect == None
        # ignore if minimum curvature is below the thermocline
        if(i_1 < i_2):
            z_top_intersect = None
        # assume mixed to max(depths) if Ts-Tb < min.dT
        if(abs(T_surface_mean-T_bottom_mean) < dT_min):
            z_top_intersect = max(z) 
        
        
        #results[i]["z_mix"]
        results[i]["z_epi_bottom"] = z_top_intersect
        results[i]["z_hyp_top"] = z_bottom_intersect
                    
    results[i]["strongest gradient"]    = z[i_1]
    results[i]["minimum curvature"]     = z[i_2]
    results[i]["maximum curvature"]     = z[i_3]
    
    results[i]["T"] = T
    results[i]["z"] = z    
        

    return

#=====================================================================================================================
# plotting
#=====================================================================================================================

#results[i]["profile_fittiing"]

plt.plot(temp[203],z[203])
    
i = 203 #203
plotting(results[i]["T"],
        results[i]["z"],
        results[i]["z_epi_bottom"],
        results[i]["z_hyp_top"],
        *results[i]["coef"]
        )


def plotting(results, i):
    
     Rs = results[i]
    
    surf_SLOPE, surf_INTER, thermo_SLOPE, thermo_INTER, bot_SLOPE, bot_INTER
    
    
    
    fig = plt.figure(figsize=(7,8))
    plt.clf() 
    axes1 = fig.add_subplot(111)
            
    axes1.plot(Rs["T"],Rs["z"], label = "Temp °C", color = "black")
    axes1.xaxis.set_label_position('bottom')
    axes1.set_xlabel("Temp °C", color = "black")
    
    #axes1.axhline(y = z[i_1], label = "strongest gradient", color = "blue")
    #axes1.axhline(y = z[i_2], label = "minimum curvature (bot epilimnion)", color = "red")
    #axes1.axhline(y = z[i_3], label = "maximum curvature (top hypolimnion)", color = "green")
    
    
    "strongest gradient (i1)": None,
    "minimum curvature(i2)": None,
    "maximum curvature(i3)": None,
    
    
    
    axes1.axhline(y = Rs["z_top_intersect, label"] = "thermocline top", color = "red")
    axes1.axhline(y = Rs["z_bottom_intersect, label"] = "thermocline bottom", color = "blue")
    
    
    abline(Rs["surf_SLOPE"], Rs["surf_INTER"])
    abline(Rs["thermo_SLOPE"], Rs["thermo_INTER"])
    abline(Rs["bot_SLOPE"], Rs["bot_INTER"])
     
    #plt.plot(temp_in, reg_surf_intercept + reg_surf_slope*temp_in, 'r', label='fitted line')
    
    axes1.set_xlim(min(Rs["T"])*0.9, max(Rs["T"])*1.1)
    axes1.set_ylim(min(Rs["z"])*0.9, max(Rs["z"])*1.1)
    
    
    ## fitted profile
    # create data for the fitted equation plot
    #xModel = numpy.linspace(min(xData), max(xData))
    #yModel = func1(xModel, *fittedParameters)
    
    # now the model as a line plot
    #axes1.plot(T_fitted_profile,z)
    #axes1.axhline(y = zthermocline, label = "zthermocline", color = "orange")
    
    plt.legend()
    
    plt.gca().invert_yaxis()
    
    """
    axes3 = axes1.twiny()
    axes3.scatter(cur,depth_in, label = "ODO mg/L", color = "black")
    axes3.xaxis.set_label_position('top')
    axes3.set_xlabel("DO mg/l", color = "blue")
    """
    
    return axes1








#temp_given_time_step = np.asarray(temp)
#density_given_time_step = np.asarray(density)
#depth_given_time_step = z







#=============================================================================#

def nth_val(a, method = "sml"):
    
    if method == "sml":
        n = 2
        return np.argpartition(a, n)[n]
    if method == "lrg":
        return np.argpartition(a, -2)[-2]
    


def linear_regression(x,y):
    
    slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
    
    return slope, intercept


def get_intersection(slope1,intersect1,slope2,intersect2):

    if slope1 == slope2:
        print ("Lines are parallel!!!")
        return None
    else:
        x_intersect_value = (intersect2 - intersect1) / (slope1 - slope2)
        y_intersect_value = slope1 * x_intersect_value + intersect1
    return y_intersect_value




def abline(slope, intercept):
    """Plot a line from slope and intercept"""
    axes = plt.gca()
    x_vals = np.array(axes.get_xlim())
    y_vals = intercept + slope * x_vals
    plt.plot(x_vals, y_vals, '--')






# calculating curve details:
def get_gradient_and_curvature(T, R, z, method = 2):

    if method == 1:

        dTdz = np.diff(T) / np.diff(z)
        
        dRdz = np.diff(R) / np.diff(z)
        
        curT = np.diff(np.diff(T)) / np.diff(z,n=2)**2

    if method == 2:
        # first derivatives
        dz = np.gradient(z)
        dTdz = np.gradient(T) / dz
        
        dRdz = np.gradient(R) / dz

        # second derivatives
        d2z = np.gradient(dz)  
        d2T = np.gradient(dTdz)
        
        # curvature
        curT = (d2T) / (np.sqrt(1 + dTdz ** 2)) ** 1.5  
        #d2T_dz2 =  d2T / dz**2

    return curT, dTdz, dRdz








"""

 # first derivatives
dz = np.gradient(z, z)
dT = np.gradient(T, z)

# second derivatives
d2z = np.gradient(dz, z)  
d2T = np.gradient(dT, z)

T_cur = (d2T) / (np.sqrt(1 + dT ** 2)) ** 1.5  # curvature

d2T_dz2 =  d2T / dz**2


i_1 = np.argmin(dT) # index of strongest gradient (-ve for positive strat, +ve for winter strat)
i_2 = np.argmin(d2T_dz2) # index of minimum curvature (bot epilimnion)
i_3 = np.argmax(d2T_dz2) # index of maximum curvature (top hypolimnion)


dx = np.gradient(depth_in)
dy = np.gradient(temp_in)

d2x = np.gradient(dx)
d2y = np.gradient(dy)

curvature = abs(dx * d2y - d2x * dy) / (dx * dx + dy * dy) ** 1.5
"""


