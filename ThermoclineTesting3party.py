# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 08:35:20 2021

@author: au302246
"""

# reference:
# https://github.com/stormxuwz/SeabirdCode
"""
Lake Stratification features

    TRM_gradient_segment: the gradient of temperature at TRM (unit C/m)
    TRM_segment: the depth of thermocline using PLR method
    UHY_segment: the depth of upper hypolimnion
    LEP_segment: the depth of the lower epilimion
    doubleTRM: whether double thermocline exist
    positiveGradient: whether abnormal temperatuer increasing with depth exists
    firstSegmentGradient: the gradient of the first segment (C/m)
    lastSegmentGradient: the gradient of the last segment (C/m)

Piecewise linear segmentation

Schmidts Stability:
    measure of the energy required to completely mix a stratified lake with an arbitrary vertical density distribution

    Schmidt stability, or the resistance to mechanical mixing due to the potential energy inherent in the
    stratification of the water column.
    Schmidt stability was first defined by Schmidt (1928) and later modified by Hutchinson (1957).
    This stability index was formalized by Idso (1973) to reduce the effects of lake volume on the
    calculation (resulting in a mixing energy requirement per unit area)

"""
# standard packages:
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objs as go
import plotly
import netCDF4
import os.path
import sys
import json
import datetime
import pickle


# locale packages:
import thermocline_utils as t_utils
import thermocline_analysis as t_analysis
import thermocline_plotting as t_plot


# Switch on debugging:
debugging = False #True

#-----------------------------------------------------------------------------#
# reading CONFIGURATIONS
#-----------------------------------------------------------------------------#

config=json.load(open('configOBSTempChain.json'))

#------------------checking datetime and period to extract--------------------#

dtDict = t_utils.checkingDatetime(config)

if dtDict == None:
    print("Analysis was cancelled...")

else:
    #-----------------------------datatype to analyze-------------------------#
    datasetType = config["Configuration"]["datasetType"]
    print("Data configured to be analyzed: %s" %datasetType)

    #--------------------------Definition of paths----------------------------#

    #for debugging:
    if debugging == True:
        root = os.getcwd()
        # plotting root:
        plotsOut= os.path.join(root, "DATA/Plots")
    
        # observation examples:
        #dataset = os.path.join(root,"DATA/OBS_WinterExample/Winter.obs")
        
        dataset = os.path.join(root, "DATA/OBS_LakeRavn/TEMPselected.obs")
        datasetHYPS = os.path.join(root, "DATA/OBS_LakeRavn/hypsograph.dat")
        
        #datasetdataset = os.path.join(root, "DATA/OBS_Relab/tempNewPeriod.obs")
        #datasetHYPS = os.path.join(root, "DATA/OBS_Relab/hypsograph.dat")
        
        # model examples
        #dataset = os.path.join(root, "DATA/MODEL_LakeBryrup/output.nc")
        #dataset = os.path.join(root, "DATA/MODEL_LakeRavn/output.nc")

    elif debugging == False:
        plotsOut = config["Configuration"]["paths"]["plotsOut"]
        datasetHYPS = config["Configuration"]["paths"]["datasetHYPS"]
        dataset = config["Configuration"]["paths"]["dataset"]


#------------------------------Analyzing Data---------------------------------#

if datasetType == "obs":
    analyzedData = t_analysis.analyzeData(config, dtDict, datasetType, dataset, datasetHYPS)

if datasetType == "model":
    analyzedData = t_analysis.analyzeData(config, dtDict, datasetType, dataset)

#-----------------------------Exporting data----------------------------------#
# export results to CSV
#-----------------------------------------------------------------------------#

t_utils.exportResults(analyzedData,plotsOut)

# export main data dictionary to pickle file for later import

with open(os.path.join(plotsOut,'results.pickle'), 'wb') as handle:
    pickle.dump(analyzedData, handle, protocol=pickle.HIGHEST_PROTOCOL)

#-----------------------------importing data----------------------------------#

with open(os.path.join(plotsOut,'results.pickle'), 'rb') as handle:
    analyzedData = pickle.load(handle)

#-----------------------------------------------------------------------------#

#-----------------------------------------------------------------------------#
# plotting
#-----------------------------------------------------------------------------#
# Do timeseries plot
# let the user choose which date to plot

targetDate = "2020-08-15 23:00:00" # "all"

t_plot.initIndividualPlotting(analyzedData, targetDate, plotsOut)

t_plot.plotly_temporal(plotsOut, analyzedData, var = "stability") # var = "mix" or "stability"


