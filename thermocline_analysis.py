# -*- coding: utf-8 -*-
"""

"""
import pandas as pd
import numpy as np
import netCDF4
import os.path
import sys
import json
import datetime

# locale packages:
from packages.Seabird.seabird.seabird_class import seabird
import thermocline_utils as t_utils
import schmidt_stability as st

def analyzeData(config, dtDict, datasetType, dataset, datasetHYPS = None):
    
    # initial flags
    FlagCalculateSchmidtsStability = True

    if datasetType == "obs":
        ###########################################################################
        # Obs files
        ###########################################################################
           
        # import OBS profiles and hypsograph data
        try:
            obsData = t_utils.read_obs_format(dataset)
            
            datetime_fmt        = obsData["obsTime"]
            zImport             = obsData["obsDepth"]
            tempImport          = obsData["obsValue"]
            densityCalculated   = t_utils.water_density(tempImport)
            
            # removin profiles with too few observations:
            zImport = pd.DataFrame(zImport)
            Bool = zImport.dropna(thresh=config["Configuration"]["profileThreshold"]).index.values.tolist()
                                 
            z = zImport.iloc[Bool].to_numpy() *-1
                    
            tempImport = pd.DataFrame(tempImport)
            temp = tempImport.iloc[Bool].to_numpy()
            
            densityCalculated = pd.DataFrame(densityCalculated)
            density = densityCalculated.iloc[Bool].to_numpy()
            
            datetime_fmt = pd.DataFrame(datetime_fmt)
            datetime_fmt = datetime_fmt.iloc[Bool].values.astype('datetime64[s]').flatten()
            datetime_fmt =  datetime_fmt.astype(datetime.datetime)
            
            #-------------truncating the data for the given period----------------#
            if dtDict["FlagPeriod"] == "period":
                valid_dt = np.array([dtDict["dateExtractStart"] <= t <= dtDict["dateExtractStop"] for t in datetime_fmt], dtype=bool)
                
                datetime_fmt    = datetime_fmt[valid_dt]
                z               = z[valid_dt]
                temp            = temp[valid_dt]
                density         = density[valid_dt]
                
                
            NoneType = type(None)      
            if type(datasetHYPS) != NoneType:
                if os.path.exists(datasetHYPS):
                
                    hypsDepth, hypsArea = t_utils.ReadHypsograph(datasetHYPS)
                    hypsDepth = hypsDepth[-1]-hypsDepth 
                                       
                else:
                    print("The hypsograph file does not exist. Hence, calculations of Schmidt stability will not be performed")
                    FlagCalculateSchmidtsStability = False
            
            else:
                print("No hypsograph has been specified. Hence, calculations of Schmidt stability will not be performed")
                FlagCalculateSchmidtsStability = False    
            
        
        except Exception as e:
            print("The format of the .obs file is not correct")
            print(e)
            
            return None
        
        
    if datasetType == "model":
        ###########################################################################
        # NC files
        ###########################################################################
        #dataset = "C:/workspaceLocal/lakeinsight_physicalcharacteristics/DATA/MODEL_LakeRavn/output.nc"
        #dtDict = {}
        #dtDict["FlagPeriod"] = "all"
        with netCDF4.Dataset(dataset) as nc:
        
            try:
                nctime = nc.variables['time']
                time_vals = nctime[:]
                time_unit = nctime.units
                # Getting metadata about the simulation period:
                simulation_start = netCDF4.num2date(nctime[0], time_unit)
                simulation_stop = netCDF4.num2date(nctime[-1], time_unit)
                print("Simulation output is ranging from %s to %s" %(simulation_start,simulation_stop))
                
                # Getting the full time-dimention
                datetime_fmt_complete = (netCDF4.num2date(time_vals, time_unit))
                
                if dtDict["FlagPeriod"] == "period":
                    valid_dt = np.array([dtDict["dateExtractStart"] <= t <= dtDict["dateExtractStop"] for t in datetime_fmt_complete], dtype=bool)            
                
                if dtDict["FlagPeriod"] == "all":
                   valid_dt = np.array(datetime_fmt_complete, dtype=bool) 
                                      
                # Needed variables are extracted and truncated at the same time
                z               = nc.variables["z"][valid_dt,:, 0, 0] *-1#[..., 0, 0] 
                h               = nc.variables["h"][valid_dt,:, 0, 0]#[..., 0, 0]
                temp            = nc.variables["temp"][valid_dt,:, 0, 0]#[..., 0, 0]
                density         = nc.variables["rho"][valid_dt,:, 0, 0]#[..., 0, 0]
                sal             = nc.variables["salt"][valid_dt,:, 0, 0]#[..., 0, 0] # g/kg
                bthA            = nc.variables["Af"][valid_dt,:, 0, 0]#[..., 0, 0] # m2, hypsograph at grid interface
                depths          = z #* -1
                #bthD = depths # m, bathymetry depths is identical to z in model
                mixed_bot       = nc.variables["mld_bott"][valid_dt, 0, 0]#[..., 0, 0] # bottom mixed layer depth (m)
                mixed_surf      = nc.variables["mld_surf"][valid_dt, 0, 0]#[..., 0, 0] # surface mixed layer depth (m)
                # TODO: compare mixed_bot with z_mix
                #surf_fric = nc.variables["u_taus"][..., 0, 0] # surface water friction (due to wind?) (m/s)            
                
                # we use the cummulated layer height to get the "depths" as it
                # accounts for simulations with varying water level
                h_cumsum = h.cumsum(axis=1)
                # Centres (all)
                depthsAccum = np.abs(h_cumsum - h_cumsum[:, -1, np.newaxis] - h/2)
                               
                #depths  = np.flip((depthsAccum.transpose()[0,:]-depthsAccum.transpose()).transpose())
                
                #depths = (depthsAccum.transpose()[0,:]-depthsAccum.transpose()).transpose()
                # we set z to depths:
                z = depthsAccum
                
                datetime_fmt    = datetime_fmt_complete[valid_dt]
                            
            except Exception as e:
                print("Something went wrong while trying to extract data from the model simulation")
                print(e)
            
                return None
                

    #-----------------------------------------------------------------------------#
    # Creating dict for analysis results
    #-----------------------------------------------------------------------------#
    
    results = {}
    for i, d in enumerate(datetime_fmt): # datetime_fmt[date_mask]
        results[i] =    {
                        "datasetType"                       : datasetType,
                        "date"                              : d.strftime('%Y-%m-%d %H:%M:%S'),
                        "T_raw"                             : None,
                        "R_raw"                             : None,
                        "z_raw"                             : None,
                        "depths_raw"                        : None,  
                        "bthA_raw"                          : None,
                        "sal_raw"                           : None,
                        "sal_constant"                      : None,
                        "hypsArea"                          : None,
                        "hypsDepth"                         : None, 
                        "analysis_GOTM"                     : None,
                        "analysis_SCHMIDTS"                 : None,
                        "analysis_THERMOCLINE"              : None,
                        "FlagCalculateSchmidtsStability"    : FlagCalculateSchmidtsStability,
                        
                        }
    
    #-----------------------------------------------------------------------------#
    # Analysis
    #-----------------------------------------------------------------------------#
    
    #for i, (T_raw, R_raw, z_raw) in enumerate(zip(temp, density, depths)):
    for i, (T_raw, R_raw, z_raw) in enumerate(zip(temp, density, z)):
        print("==============================================")
        print("ID: %s" %i)
        print("==============================================")
        print("Date is : %s" %results[i]["date"])
        # the raw datasets are saved in the dict for later plotting. this is
        # only relevant for obs imports to ensure same length
    
        results[i]["T_raw"] = T_raw
        results[i]["R_raw"] = R_raw
        results[i]["z_raw"] = z_raw
        
        #-------------------------------------------------------------------------#
        # specifically for OBS data:
        if results[i]["datasetType"] == "obs":
            if 'hypsDepth' in locals():
                results[i]["hypsDepth"] = hypsDepth
            
            if 'hypsArea' in locals():
                results[i]["hypsArea"] = hypsArea
        #-------------------------------------------------------------------------#    
        # specifically for MODEL data:
        if results[i]["datasetType"] == "model":
            results[i]["depths_raw"]    = depths[i]
            results[i]["sal_raw"]       =  sal[i]
            results[i]["bthA_raw"]      = bthA[i]
           
        #-------------------------------------------------------------------------#
        # GOTM results
        #-------------------------------------------------------------------------#
        if results[i]["datasetType"] == "model":
            # extract results from model
            results[i]["analysis_GOTM"] = {'mixed_surf'        : mixed_surf[i],
                                           'mixed_bot'         : mixed_bot[i],
                                           'mixed_layer_depth' : mixed_surf[i]
                                           }
            
        
        ###########################################################################
        # OTHER CALCULATIONS
        ###########################################################################
        
        #-------------------------------------------------------------------------#
        # deleting possible nan values.
        T_ForCal = T_raw[~np.isnan(T_raw)]
        z_ForCal = z_raw[~np.isnan(z_raw)]
        R_ForCal = R_raw[~np.isnan(R_raw)]
       
        
        #-------------------------------------------------------------------------#
        # Schmidts stability results
        #-------------------------------------------------------------------------#
        if results[i]["datasetType"] == "obs":
            if results[i]["FlagCalculateSchmidtsStability"] == True:
                        
                results[i]["analysis_SCHMIDTS"] = st.schmidt_stability(config, datasetType, T_ForCal, z_ForCal, hypsArea, hypsDepth, sal = 0, rhoL_in = R_ForCal)[0]
                #print("Schmidt stability calculated.")
            
        
        if results[i]["datasetType"] == "model":
                  
            # calculate stablity
            depths_raw = depths[i]
            sal_raw = sal[i]
            bthA_raw = bthA[i]
            results[i]["analysis_SCHMIDTS"] = st.schmidt_stability(config, datasetType, T_ForCal, depths_raw, bthA_raw, depths_raw, sal = sal_raw, rhoL_in = R_raw)[0]
            #print("Schmidt stability calculated.")
    
    
        #-------------------------------------------------------------------------#
        # Thermocline analysis
        #-------------------------------------------------------------------------#
        """
        Based on a modified version of the paper:
        Algorithmic Characterization of Lake Stratification and Deep Chlorophyll 
        Layers From Depth Profiling Water Quality Data
        Paper DOI: 10.1029/2018WR023975
        """
        
        # configuring a dataframe (targeting the format of the algorithm) 
        
        df_ForAnalysis = pd.DataFrame({"Depth": z_ForCal,
                                       "Temperature": T_ForCal
                                      })
               
        df_ForAnalysis["Depth"] = df_ForAnalysis["Depth"]#*-1
        
        df_ForAnalysis = df_ForAnalysis.sort_values("Depth")    
    
        
        #config=json.load(open('config.json'))
        mySeabird = seabird(config = config)  # config is in json format
        mySeabird.loadData(dataFile = df_ForAnalysis)
        downCastRawData, cleanData =mySeabird.preprocessing()
        NoneType = type(None)
           
        if type(downCastRawData) == NoneType or type(cleanData) == NoneType:
            print("Data is not valid for thermocline analysis")
        else:
            features = mySeabird.identify()
            #print(features)
            #features = mySeabird.features  # features are in dictionary format.
        
            results[i]["analysis_THERMOCLINE"] = features
            results[i]["analysis_THERMOCLINE"].update({'cleanData': cleanData})
            results[i]["analysis_THERMOCLINE"].update({'downCastRawData': downCastRawData})
   

    return results    
