# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 17:39:50 2021

@author: au302246
"""


#packages to import:
import pandas as pd
import os
import os.path
import os
import numpy as np

obsDataset = "O:/Tech_DYAMO-DK/07_CODE/Thermocline/DATA/obs/TEMP.obs"
parameter = "Temperature"

###############################################################################
# Importing the observed chemistry 
###############################################################################

from collections import defaultdict

def read_autocalibration_format(obsDataset):

    dic = defaultdict(lambda: defaultdict(list))
    obsTime = []
    obsDepth = []
    obsValue = []     
    
    input_file = obsDataset #os.path.join(inputfolder, file)    
    
    with open(input_file,'r') as f:
        lines = f.readlines()
        #dic = {}
        for line in lines:
            if line.strip()=='': break
            
            time = line[:19]
            z,val = line[19:].strip().split()
                       
            dic[time]["depth"].append(float(z))
            dic[time]["val"].append(float(val))
    
    for key, val in dic.items():
        obsTime.append(key)
        obsDepth.append(np.array(val["depth"]))
        obsValue.append(np.array(val["value"]))

    return obsTime, obsDepth, obsValue 